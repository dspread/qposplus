#ifndef _TMSCLIENT_H
#define _TMSCLIENT_H

#ifdef __cplusplus
extern "C"
{
#endif//__cplusplus
#define SECTOR_SIZE 					0x1000
#define MAX_SIZE_OTA_USED				0x400000	//0 ~ 4MB, 1st sec for ota info
#define OTA_ROM_CACHE_ADDR				TMS_FW_UPDATE_ADDR	//ext Rom from 0x1000 occuppoed by OTA, ~8MB size
#define MAX_SIZE_ORIGINAL_OTA			(MAX_SIZE_OTA_USED - OTA_ROM_CACHE_ADDR)	//original ota, size (8MB - 4KB)
#define MAX_SECTORS_ORIGINAL_OTA		(MAX_SIZE_ORIGINAL_OTA/SECTOR_SIZE)	//original ota, 2047 sectors
#define BYT_SEC(size)					(((size) + SECTOR_SIZE - 1)/SECTOR_SIZE)
#define SECTORS_OTA_DEFAULT				(0x100 - 1)
#define SIZE_OTA_DEFAULT				(SECTOR_SIZE*SECTORS_OTA_DEFAULT)	//gzip file, size FF000
#define EXT_SECTORS(size)				((size) > SIZE_OTA_DEFAULT ? BYT_SEC(size - SIZE_OTA_DEFAULT) : 0)
#define UNZIP_ADDR_DEFAULT				(OTA_ROM_CACHE_ADDR + SIZE_OTA_DEFAULT)	//1M



#define UNZIP_TO_ADDR(ota_size)			((OTA_ROM_CACHE_ADDR + (ota_size) + SECTOR_SIZE - 1)/SECTOR_SIZE*SECTOR_SIZE)
#define MAX_SECTORS_WIPE				((MAX_SIZE_OTA_USED - UNZIP_ADDR_DEFAULT)/SECTOR_SIZE)

#undef  UPG_BLOCK_SIZE
#define UPG_BLOCK_SIZE(CUT) 			0	//change into 0 for modem transparent mode
#define SIZE_ARRAY(x)  					(sizeof(x)/sizeof(*x))
#define membsizeof(type, member) 		sizeof(((type *)0)->member)
#define WIFI_SEND_LEN_MAX        		(2048)
#define HTTP_FRAME_LENGTH_MAX       	(4096)
#define WIFI_HTTP_DOWN_LEN_MAX        	(2048)	//n*512 relied on WiFiSocket
#define NULL_NET_STRATEGY				MAX_NET_STRATEGY
#define MAX_PATCH_DOWNLOAD				1
#define LEN_REVISION					40
#define LEN_SHORT_REVISION				10
#define LEN_PATCH_FILE_NAME				40
#define LEN_POS_SN						20
#define BAD_PATCH						0xFF
#define STR_RESULT(x) 					(x == RC_FAIL ? "RC_FAIL" :(x == RC_QUIT ? "RC_QUIT" : "RC_SUCCESS"))
#define LEN_CAIDC						15
#define U8TOSZ(x)						((char[] ){((x)>>4|0x30), ((x)&0x0F)|0x30, 0})
#define STR_DOWNING(x)					((x) == BAD_PATCH ? "NONE" : U8TOSZ(x))
#define TMS_HEART_PATH 					"/api/v1/heartbeat?"
#define REVISION2PNAME(arr)				((char[]){arr[0], arr[1], arr[2], arr[3], arr[4], arr[5], arr[6], arr[7], '.', arr[8], arr[9], 0})
#define PFILE_NAME(db, i)				REVISION2PNAME(db->patch[i].szRev)
#define S_REVISION(arr)					((char[]){arr[0], arr[1], arr[2], arr[3], arr[4], arr[5], arr[6], arr[7], arr[8], arr[9], 0})
#define PRT_PATCH(p)					\
										do{	\
											u8 byIdxM;	\
											TRACE(DBG_TRACE_LVL, "sn %s, merchant %s, patches %d, path: %s\r\n", (p)->szPosSn, (p)->szMerchantId, (p)->byTotal, (p)->szDnReqHead);	\
											for(byIdxM = 0; byIdxM < (p)->byTotal; byIdxM++)	\
												TRACE(DBG_TRACE_LVL, "<%d> repo:%d, %s, %s, ZIP:%d, %s, attach:%s\r\n", byIdxM, (p)->patch[byIdxM].repo, 	\
												STR_PATCH_TYPE[(p)->patch[byIdxM].type], STR_DN_STATE[(p)->patch[byIdxM].eState], (p)->patch[byIdxM].bZip,	\
												S_REVISION((p)->patch[byIdxM].szRev), (p)->patch[byIdxM].szAttach);	\
										}while(0)
#define RTC_DATETIME					({u8 a[16] = {0}; lark_rtc_read(a);	\
											((char[] ){a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7], a[8], a[9], a[10], a[11], a[12], a[13], a[14], 0});	\
										})
#define XFLASH_ERASE_SECTORS(addr, n)	lark_xflash_write(addr, NULL, n*SECTOR_SIZE)
#define IS_HEX_CHAR(x) 					(((x) >= 'A' && (x) <= 'F') || ((x) >= 'a' && (x) <= 'f') || ((x) >= '0' && (x) <= '9'))
#define UV_NULL(x) 						(!(x).head && !(x).len)
#ifdef CHUNK_GZIP
	#define ACCEPT_ENCODE				"Accept-Encoding: gzip, deflate\r\n"
#else
	#define ACCEPT_ENCODE				""
#endif
#define SZ_ASC_GZ						".asc.gz"

typedef struct{
	u8 *head;
	u32 len;
	u32 offset;
	u32 dwCrcWrote;
	u8 bEraseNeeded : 1;
	u8 bUnzipNeeded : 1;
	u8 : 6;
} T_CACHE;

typedef struct {
	u32 dwTag;
	char *strJsonKey;
    char *strJsonValue;
} T_FIELD_DEF;

typedef enum{
	DN_FORCE,
	DN_OPTIONAL,
	DN_END,
	UPG_OK
} T_DOWN_STATE;

#define OTA_FAIL			DN_FORCE
#define OTA_INPROGRESS		DN_OPTIONAL
#define OTA_END				DN_END
#define OTA_RESET_UPDATE	UPG_OK

typedef enum{
	HTTP_GET,
	HTTP_POST,
	HTTP_PUT,
	HTTP_DELETE
}T_HTTP_METHOD;
static const char *strMethod[] = {"GET", "POST", "PUT", "DELETE"};

typedef enum{
	FIRMWARE,
	OTHER,
	OTA,
	ROM,
	PATCH_TYPE_BAD
} T_PATCH_TYPE;

typedef enum{
	ENC_NONE,
	ENC_CHUNKED,
	ENC_GZIP,
	ENC_DEFLATE,
	ENC_TOTAL
} T_ENC_CONTENT;
static const char *strEncContent[] = {"ENC_NONE", "ENC_CHUNKED", "ENC_GZIP", "ENC_DEFLATE", "ENC_USER"};

#pragma pack(push)
#pragma pack(1)
typedef struct{
	u32 dwOffset;
	u32 dwLenBlk;
	u32 dwLenContent;
	u32 dwLenFile;
	u32 dwETag;
	u16 wStatus : 13;
	u16 bEncoding : 3; //T_ENC_CONTENT
}T_BLOCK_HEAD;

typedef struct{
	u8 repo 			:1;
	u8 type				:4;
	u8 eState			:2;
	u8 bZip				:1;
	char szRev[LEN_REVISION + 1];
	char szAttach[LEN_PATCH_FILE_NAME + 1];
} T_PATCH_INFO;

typedef struct{
	u32 	dwCrcSelf;
	char 	szDnReqHead[128];
	char 	szPosSn[LEN_POS_SN + 1];
	char 	szTerminalNum[LEN_CAIDC + 1];
	char 	szMerchantId[LEN_CAIDC + 1];
	//char	szKdhServer[128];
	u32 	dwCrcKdhCert;
	u32 	dwHeartbeat;
	u32 	dwUpdate;
	u8 		byTotal;
	u8 		byDowning;
	u32 	dwOffset;
	u32 	dwLenFile;
	u32		dwETag;
	u32 	dwCrcPatial;		//Partial CRC for downloading file
	T_PATCH_INFO patch[MAX_PATCH_DOWNLOAD];
} T_TMS_DATA;

typedef struct{
	char cRepo;
	char cSplitLeft;
	char arrRevision[LEN_REVISION];
	char cSplitRight;
	char cForce;
}T_REVSION_BLK;
#pragma pack(pop)

typedef enum{
	NUMERIC,
	SZARRAY,
	REVISION
}T_FIELD_TYPE;

typedef struct{
	char *strJsonKey;
	u32 dwOffset;
	T_FIELD_TYPE eType;
	u32 dwSize;
} T_FIELD_MAP;

typedef struct{
	u32 dwLenPending;
	u8 byFlagHead : 7;
	u8 byFlagTail : 1;
	u32 dwLenLeft;
}T_CHUNK_PARTIAL;

typedef enum {
	OTA_NORMAL = 0,
	OTA_DETECT
} T_OTA_OPTION;

#include "mdmsock.h"

extern T_URL_INFO* parse_url(T_URL_INFO* info, char* strUrl);
extern char *uv2str(char *strRet, T_U8_VIEW *uv);
extern s32 WiFiSocket(T_URL_INFO *pUrlInfo, T_U8_VIEW uvSend, T_U8_VIEW uvRecv);
extern char *str_in_uv(T_U8_VIEW uvSrc, char *sub);
extern char *trim(char *s, char cExt);
extern u32 strcmp_case(char *src, char *sub, bool sensitive);
extern char *splitstr(char *line, const char *search);
extern char *str_in_uv(T_U8_VIEW uvSrc, char *sub);
extern u32 str2num(char *strAmt, u32 dwLen);
extern u32 uv2asc(u8 *pAsc, T_U8_VIEW uvBin);
extern char *replacestr(char *line, const char *search, const char *replace);
extern void CjsonPortingInit(void);
extern void OTA_Upgrade(void);
extern int modem_open(void);
extern int modem_sock(T_URL_INFO *pUrlInfo, T_U8_VIEW uvSend, T_U8_VIEW uvRecv);
extern int modem_close(void);
extern int close_mdm_sock(void);
extern void debug_uart_info(void);
extern int obtain_sock_type(void);
extern int detect_modem(T_MODEM_SELECT select);
extern u32 WaitEvents(u32 events, u32 timeout, pvoid pbuffer);
extern Rc_t tms_client_check(const char *szUrl);
extern void set_user_appver(char * ver);//No spaces, no more than 15 characters

#endif

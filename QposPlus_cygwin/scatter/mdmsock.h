#ifndef _MDMDOCK_H
#define _MDMDOCK_H

#ifdef __cplusplus
extern "C"
{
#endif//__cplusplus

#define DW_MAX								(0xFFFFFFFF)
#define IS_DISPLAYABLE(x)					((x)>= 0x20 || (x)<=0x7E)
#define membsizeof(type, member) 			sizeof(((type *)0)->member)
#define WIFI_SEND_LEN_MAX        			(2048)
#define HTTP_FRAME_LENGTH_MAX       		(4096)
#define WIFI_HTTP_DOWN_LEN_MAX        		(2048)	//n*512 relied on WiFiSocket
#define UPG_BLOCK_DLT 						(220)

#define TO_UPPER(c) 						((c)-32*((c)>='a' && (c)<='z'))
#define CHAR2BIN(c)							(((c)&0x7F) >= 'a' ? (((c)&0x7F) - 'a' + 10) \
											: ((((c)&0x7F) >= 'A') ? (((c)&0x7F) - 'A' + 10) : (((c)&0x7F) - '0')))
#define SIZE_ARRAY(x)  						(sizeof(x)/sizeof(*x))
#define IS_SPACE(c, x)  					\
											((c) == ' ' || (c) == '\t' || (c) == '\r' || (c) == '\n' || (c) == '\v' || (c) == '\f' \
											|| (x != '\0' && (c) == x))
#define EVENT_TIMEOUT_MS 					(500)
#define DEMO_TIMEOUT_SEC      				(10)
#define NET_TIMEOUT_SEC      				(5)
#define PACKET_TIMEOUT_MS       			(EVENT_TIMEOUT_MS*5)
#define PTR_OF_DATA(buf)					(buf + 7)
#define RET_CODE(buf)						(buf[6])
#define PTR2WORD(x)							(*(x)<<8|*((x) + 1))
#define PTR2DWORD(x)						(PTR2WORD(x)<<16|PTR2WORD(x + 2))
#define U16_BYTES(w)						((u8[] ){(w)>>8, w})
#define U32_BYTES(d)						((u8[] ){LONG_HH(d), LONG_HL(d), LONG_LH(d), LONG_LL(d)})
#define CHAR2LONG(c0, c1, c2, c3)			(u32)((c0)<<24|(c1)<<16|(c2)<<8|(c3))
#define WIFI_FRAME_LEN_MAX					512
#define WIFI_FRAME_REAL_LEN					(WIFI_FRAME_LEN_MAX - 8)
#define NET_ERROR_OK                       	(0)
#define NET_ERROR_GPRS_REQ_DATA            	(-1)
#define NET_ERROR_GPRS_HANDSHAKE           	(-2)
#define NET_ERROR_GPRS_USER_CANCEL         	(-3)
#define NET_ERROR_GPRS_FRAME_LENGTH        	(-4)
#define NET_ERROR_WIFI_REQ_DATA            	(-5)
#define NET_ERROR_WIFI_WRITE_DATA          	(-6)
#define NET_ERROR_WIFI_DES_WRITE_DATA      	(-7)
#define NET_ERROR_WIFI_DES                 	(-8)
#define NET_ERROR_WIFI_USER_CANCEL         	(-9)
#define NET_ERROR_WIFI_FRAME_LENGTH        	(-10)
#define NET_ERROR_GPRS_CONNECT             	(-11)
#define NET_ERROR_REQEST_PARAM             	(-12)
#define NET_ERROR_BAD_STRATEGY             	(-13)
#define NET_ERROR_BAD_SERVER             	(-14)
#define NET_ERROR_SEND_FAIL             	(-15)
#define NET_ERROR_RECV_FAIL             	(-16)
#define NET_ERROR_OP_TIMEOUT             	(-17)
#define NET_ERROR_BAD_FRAME             	(-20)
#define NET_ERROR_BUF_FULL             		(-21)
#define NetErrorTransmit(x) 			   	(x - 100)
#define MDM_PORT_BAUD_RATE					(115200*4)
#define CELL_SOCK_DIS_PATTERN				(u8[]){'\r','\n','C','L','O','S','E','D','\r','\n'}

typedef enum{
	WIFI_SOCK,
	CELL_SOCK,
	SOCK_NONE
}T_SOCKET_INTERFACE;

typedef enum{
	MODEM_SEL_AUTO = 0,
	MODEM_SEL_WIFI = 1,
	MODEM_SEL_CELL = 2
}T_MODEM_SELECT;

#define CELL_PORT	2
#define WIFI_PORT	4
typedef int 		(*FUNC_OPEN)(int baud_rate);
typedef int 		(*FUNC_CLOSE)(void);
typedef int  		(*FUNC_CTS)(int enable);
typedef int  		(*FUNC_NET_INFO_LNK)(void);
typedef int 		(*FUNC_NET_CONN)(int type, char *host, unsigned short port, int timeout);
typedef int 		(*FUNC_NET_WRITE)(int socket, unsigned char *data, int len, int timeout);
typedef int 		(*FUNC_NET_READ)(int socket, unsigned char *buf, int max_len, int timeout);
typedef int 		(*FUNC_NET_DISCONN)(void);

typedef struct{
	T_SOCKET_INTERFACE eType;
	FUNC_OPEN 			pfunOpen;
	FUNC_CLOSE			pfunClose;
	FUNC_CTS			pfunCts;
	FUNC_NET_INFO_LNK	pfunNetInfoLnk;
	FUNC_NET_CONN		pfunNetConnect;
	FUNC_NET_WRITE		pfunNetWrite;
	FUNC_NET_READ		pfunNetRead;
	FUNC_NET_DISCONN	pfunNetDisconnect;
} T_COM_DEV;

typedef struct{
	int com_port;
	T_COM_DEV modem;
} T_COM_PORT;

extern int modem_open(void);
extern int modem_sock(T_URL_INFO *pUrlInfo, T_U8_VIEW uvSend, T_U8_VIEW uvRecv);
extern int modem_close(void);
extern int close_mdm_sock(void);
extern void debug_uart_info(void);
extern char *str_in_uv(T_U8_VIEW uvSrc, char *sub);
extern u32 str2num(char *strAmt, u32 dwLen);
extern void uart_info(int port);
extern int detect_modem(T_MODEM_SELECT select);

#endif

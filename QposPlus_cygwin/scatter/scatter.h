/*
********************************************************************************
*
*   File Name:
*       scatter.h
*   Author:
*       SW R&D Department
*   Version:
*       V1.0
*   Description:
*
*
********************************************************************************
*/

#ifndef _SCATTER_H
#define _SCATTER_H

#ifdef __cplusplus
extern "C"
{
#endif//__cplusplus

/*-----------------------------------------------------------------------------
|   Includes
+----------------------------------------------------------------------------*/
#if !defined(VM_WIN32)
#include "sys_jump_cfg.h"
#endif

/*-----------------------------------------------------------------------------
|   Macros
+----------------------------------------------------------------------------*/
#if 0
#define CFG_DEBUG_VERSION
#endif

/* scatter list 固定在RAM的开始，方便系统调用以及内存规划 */
#define SCATTER_RAM_SIZE            (1024)
#if defined(VM_WIN32)
    #define SCATTER_RAM_ADDR            (pAPIMemMapS)
#else
    #define SCATTER_RAM_ADDR            (SCATTER_RAM_ADDR_CUSTOM)
#endif

/*
    系统支持的最大应用数量
    系统应用标志使用32bits变量，如果存在一个应用，对应bit置1.
    变量为：(P_API_MEM_MAP_STRUCT->fun_status)
*/
#define SCATTER_APP_MAX_NUM         (32 - SCATTER_FUN_STATUS_APP_USER)

#define SCLOAD_PARAM_LEN            (SCATTER_RAM_SIZE)
#define SCLOAD_PARAM_ADDR           (SCATTER_RAM_ADDR)

#define P_API_MEM_MAP_STRUCT        ((pAPIMemMap )SCLOAD_PARAM_ADDR)

#define fun_status_ (P_API_MEM_MAP_STRUCT->fun_status)

#define LARK_API_ (P_API_MEM_MAP_STRUCT->LARK_API)

#define cbk_task1_ (P_API_MEM_MAP_STRUCT->cbk_task1)
#define cbk_task2_ (P_API_MEM_MAP_STRUCT->cbk_task2)
#define cbk_task3_ (P_API_MEM_MAP_STRUCT->cbk_task3)
#define cbk_com_inchar_ (P_API_MEM_MAP_STRUCT->cbk_com_inchar)
#define cbk_com_ouchar_ (P_API_MEM_MAP_STRUCT->cbk_com_ouchar)
#define cbk_assert_ (P_API_MEM_MAP_STRUCT->cbk_assert)
#define cbk_destruct_ (P_API_MEM_MAP_STRUCT->cbk_destruct)
#define cbk_systick_hook_ (P_API_MEM_MAP_STRUCT->cbk_systick_hook)
#define cbk_get_mag_key_ (P_API_MEM_MAP_STRUCT->cbk_get_mag_key)

#define Isalnum_ (P_API_MEM_MAP_STRUCT->Isalnum)
#define Isalpha_ (P_API_MEM_MAP_STRUCT->Isalpha)
#define Iscntrl_ (P_API_MEM_MAP_STRUCT->Iscntrl)
#define Isdigit_ (P_API_MEM_MAP_STRUCT->Isdigit)
#define Isgraph_ (P_API_MEM_MAP_STRUCT->Isgraph)
#define Islower_ (P_API_MEM_MAP_STRUCT->Islower)
#define Isprint_ (P_API_MEM_MAP_STRUCT->Isprint)
#define Ispunct_ (P_API_MEM_MAP_STRUCT->Ispunct)
#define Isspace_ (P_API_MEM_MAP_STRUCT->Isspace)
#define Isupper_ (P_API_MEM_MAP_STRUCT->Isupper)
#define Isxdigit_ (P_API_MEM_MAP_STRUCT->Isxdigit)
#define Tolower_ (P_API_MEM_MAP_STRUCT->Tolower)
#define Toupper_ (P_API_MEM_MAP_STRUCT->Toupper)
#define Acos_ (P_API_MEM_MAP_STRUCT->Acos)
#define Asin_ (P_API_MEM_MAP_STRUCT->Asin)
#define Atan_ (P_API_MEM_MAP_STRUCT->Atan)
#define Atan2_ (P_API_MEM_MAP_STRUCT->Atan2)
#define Cos_ (P_API_MEM_MAP_STRUCT->Cos)
#define Cosh_ (P_API_MEM_MAP_STRUCT->Cosh)
#define Sin_ (P_API_MEM_MAP_STRUCT->Sin)
#define Sinh_ (P_API_MEM_MAP_STRUCT->Sinh)
#define Tanh_ (P_API_MEM_MAP_STRUCT->Tanh)
#define Exp_ (P_API_MEM_MAP_STRUCT->Exp)
#define Frexp_ (P_API_MEM_MAP_STRUCT->Frexp)
#define Ldexp_ (P_API_MEM_MAP_STRUCT->Ldexp)
#define Log_ (P_API_MEM_MAP_STRUCT->Log)
#define Log10_ (P_API_MEM_MAP_STRUCT->Log10)
#define Modf_ (P_API_MEM_MAP_STRUCT->Modf)
#define Pow_ (P_API_MEM_MAP_STRUCT->Pow)
#define Sqrt_ (P_API_MEM_MAP_STRUCT->Sqrt)
#define Ceil_ (P_API_MEM_MAP_STRUCT->Ceil)
#define Fabs_ (P_API_MEM_MAP_STRUCT->Fabs)
#define Floor_ (P_API_MEM_MAP_STRUCT->Floor)
#define Fmod_ (P_API_MEM_MAP_STRUCT->Fmod)
#define Printf_ (P_API_MEM_MAP_STRUCT->Printf)
#define Sprintf_ (P_API_MEM_MAP_STRUCT->Sprintf)
#define Eprintf_ (P_API_MEM_MAP_STRUCT->Eprintf)
#define Atof_ (P_API_MEM_MAP_STRUCT->Atof)
#define Atoi_ (P_API_MEM_MAP_STRUCT->Atoi)
#define Atol_ (P_API_MEM_MAP_STRUCT->Atol)
#define Strtod_ (P_API_MEM_MAP_STRUCT->Strtod)
#define Strtol_ (P_API_MEM_MAP_STRUCT->Strtol)
#define Strtoul_ (P_API_MEM_MAP_STRUCT->Strtoul)
#define Abs_ (P_API_MEM_MAP_STRUCT->Abs)
#define Memchr_ (P_API_MEM_MAP_STRUCT->Memchr)
#define Memcmp_ (P_API_MEM_MAP_STRUCT->Memcmp)
#define Memcpy_ (P_API_MEM_MAP_STRUCT->Memcpy)
#define Memmove_ (P_API_MEM_MAP_STRUCT->Memmove)
#define Memset_ (P_API_MEM_MAP_STRUCT->Memset)
#define Strcat_ (P_API_MEM_MAP_STRUCT->Strcat)
#define Strncat_ (P_API_MEM_MAP_STRUCT->Strncat)
#define Strchr_ (P_API_MEM_MAP_STRUCT->Strchr)
#define Strcmp_ (P_API_MEM_MAP_STRUCT->Strcmp)
#define Strncmp_ (P_API_MEM_MAP_STRUCT->Strncmp)
#define Strcoll_ (P_API_MEM_MAP_STRUCT->Strcoll)
#define Strcpy_ (P_API_MEM_MAP_STRUCT->Strcpy)
#define Strncpy_ (P_API_MEM_MAP_STRUCT->Strncpy)
#define Strcspn_ (P_API_MEM_MAP_STRUCT->Strcspn)
#define Strerror_ (P_API_MEM_MAP_STRUCT->Strerror)
#define Strlen_ (P_API_MEM_MAP_STRUCT->Strlen)
#define Strpbrk_ (P_API_MEM_MAP_STRUCT->Strpbrk)
#define Strrchr_ (P_API_MEM_MAP_STRUCT->Strrchr)
#define Strspn_ (P_API_MEM_MAP_STRUCT->Strspn)
#define Strstr_ (P_API_MEM_MAP_STRUCT->Strstr)
#define Strtok_ (P_API_MEM_MAP_STRUCT->Strtok)
#define Strxfrm_ (P_API_MEM_MAP_STRUCT->Strxfrm)

#define tlv_new_ (P_API_MEM_MAP_STRUCT->tlv_new)
#define tlv_get_t_ (P_API_MEM_MAP_STRUCT->tlv_get_t)
#define tlv_get_t_len_ (P_API_MEM_MAP_STRUCT->tlv_get_t_len)
#define tlv_get_l_ (P_API_MEM_MAP_STRUCT->tlv_get_l)
#define tlv_get_l_len_ (P_API_MEM_MAP_STRUCT->tlv_get_l_len)
#define tlv_get_v_ (P_API_MEM_MAP_STRUCT->tlv_get_v)
#define tlv_get_node_l_ (P_API_MEM_MAP_STRUCT->tlv_get_node_l)
#define tlv_add_child_ (P_API_MEM_MAP_STRUCT->tlv_add_child)
#define tlv_find_ (P_API_MEM_MAP_STRUCT->tlv_find)
#define tlv_delete_ (P_API_MEM_MAP_STRUCT->tlv_delete)
#define tlv_modify_ (P_API_MEM_MAP_STRUCT->tlv_modify)
#define tlv_replace_ (P_API_MEM_MAP_STRUCT->tlv_replace)
#define tlv_move_ (P_API_MEM_MAP_STRUCT->tlv_move)
#define tlv_is_composite_ (P_API_MEM_MAP_STRUCT->tlv_is_composite)
#define tlv_get_first_child_ (P_API_MEM_MAP_STRUCT->tlv_get_first_child)
#define tlv_get_next_child_ (P_API_MEM_MAP_STRUCT->tlv_get_next_child)
#define tlv_batch_move_ (P_API_MEM_MAP_STRUCT->tlv_batch_move)

#define xor_nbytes_ (P_API_MEM_MAP_STRUCT->xor_nbytes)

#define Kernel_load_callback_Routine_EMV_       (P_API_MEM_MAP_STRUCT->Kernel_load_callback_Routine_EMV)
#define Kernel_Init_EMV_                        (P_API_MEM_MAP_STRUCT->Kernel_Init_EMV)
#define Kernel_Prepare_AID_List_EMV_            (P_API_MEM_MAP_STRUCT->Kernel_Prepare_AID_List_EMV)
#define Kernel_Start_EMV_                       (P_API_MEM_MAP_STRUCT->Kernel_Start_EMV)
#define Kernel_EMV_Get_ICCard_TransLog_         (P_API_MEM_MAP_STRUCT->Kernel_EMV_Get_ICCard_TransLog)
#define Kernel_EMV_Set_Element_                 (P_API_MEM_MAP_STRUCT->Kernel_EMV_Set_Element)
#define Kernel_EMV_Get_Element_                 (P_API_MEM_MAP_STRUCT->Kernel_EMV_Get_Element)
#define Kernel_Customized_Contact_EMV_          (P_API_MEM_MAP_STRUCT->Kernel_Customized_Contact_EMV)

#define Kernel_load_callback_Routine_PayPass_   (P_API_MEM_MAP_STRUCT->Kernel_load_callback_Routine_PayPass)
#define Kernel_Init_PayPass_                    (P_API_MEM_MAP_STRUCT->Kernel_Init_PayPass)
#define Kernel_Start_PayPass_                   (P_API_MEM_MAP_STRUCT->Kernel_Start_PayPass)
#define Kernel_Customized_Contactless_PayPass_  (P_API_MEM_MAP_STRUCT->Kernel_Customized_Contactless_PayPass)
#define Kernel_Set_Proprietary_Tag_PayPass_     (P_API_MEM_MAP_STRUCT->Kernel_Set_Proprietary_Tag_PayPass)

#define Kernel_load_callback_Routine_PayWave_   (P_API_MEM_MAP_STRUCT->Kernel_load_callback_Routine_PayWave)
#define Kernel_Init_PayWave_                    (P_API_MEM_MAP_STRUCT->Kernel_Init_PayWave)
#define Kernel_Start_PayWave_                   (P_API_MEM_MAP_STRUCT->Kernel_Start_PayWave)
#define Kernel_Customized_Contactless_PayWave_  (P_API_MEM_MAP_STRUCT->Kernel_Customized_Contactless_PayWave)
#define Kernel_Set_Proprietary_Tag_PayWave_     (P_API_MEM_MAP_STRUCT->Kernel_Set_Proprietary_Tag_PayWave)

#define Kernel_load_callback_Routine_AMEX_      (P_API_MEM_MAP_STRUCT->Kernel_load_callback_Routine_AMEX)
#define Kernel_Init_AMEX_                       (P_API_MEM_MAP_STRUCT->Kernel_Init_AMEX)
#define Kernel_Start_AMEX_                      (P_API_MEM_MAP_STRUCT->Kernel_Start_AMEX)
#define Kernel_Customized_Contactless_AMEX_     (P_API_MEM_MAP_STRUCT->Kernel_Customized_Contactless_AMEX)
#define Kernel_Set_Proprietary_Tag_AMEX_        (P_API_MEM_MAP_STRUCT->Kernel_Set_Proprietary_Tag_AMEX)

#define Kernel_load_callback_Routine_JCB_       (P_API_MEM_MAP_STRUCT->Kernel_load_callback_Routine_JCB)
#define Kernel_Init_JCB_                        (P_API_MEM_MAP_STRUCT->Kernel_Init_JCB)
#define Kernel_Start_JCB_                       (P_API_MEM_MAP_STRUCT->Kernel_Start_JCB)
#define Kernel_Customized_Contactless_JCB_      (P_API_MEM_MAP_STRUCT->Kernel_Customized_Contactless_JCB)
#define Kernel_Set_Proprietary_Tag_JCB_         (P_API_MEM_MAP_STRUCT->Kernel_Set_Proprietary_Tag_JCB)

#define Kernel_load_callback_Routine_Discover_  (P_API_MEM_MAP_STRUCT->Kernel_load_callback_Routine_Discover)
#define Kernel_Init_Discover_                   (P_API_MEM_MAP_STRUCT->Kernel_Init_Discover)
#define Kernel_Start_Discover_                  (P_API_MEM_MAP_STRUCT->Kernel_Start_Discover)
#define Kernel_Customized_Contactless_Discover_ (P_API_MEM_MAP_STRUCT->Kernel_Customized_Contactless_Discover)
#define Kernel_Set_Proprietary_Tag_Discover_    (P_API_MEM_MAP_STRUCT->Kernel_Set_Proprietary_Tag_Discover)

#define Kernel_load_callback_Routine_qUICS_     (P_API_MEM_MAP_STRUCT->Kernel_load_callback_Routine_qUICS)
#define Kernel_Init_qUICS_                      (P_API_MEM_MAP_STRUCT->Kernel_Init_qUICS)
#define Kernel_Start_qUICS_                     (P_API_MEM_MAP_STRUCT->Kernel_Start_qUICS)
#define Kernel_Customized_Contactless_qUICS_    (P_API_MEM_MAP_STRUCT->Kernel_Customized_Contactless_qUICS)
#define Kernel_Set_Proprietary_Tag_qUICS_       (P_API_MEM_MAP_STRUCT->Kernel_Set_Proprietary_Tag_qUICS)

#define Kernel_load_callback_Routine_qPBOC_     (P_API_MEM_MAP_STRUCT->Kernel_load_callback_Routine_qPBOC)
#define Kernel_Init_qPBOC_                      (P_API_MEM_MAP_STRUCT->Kernel_Init_qPBOC)
#define Kernel_Start_qPBOC_                     (P_API_MEM_MAP_STRUCT->Kernel_Start_qPBOC)
#define Kernel_Customized_Contactless_qPBOC_    (P_API_MEM_MAP_STRUCT->Kernel_Customized_Contactless_qPBOC)
#define Kernel_Set_Proprietary_Tag_qPBOC_       (P_API_MEM_MAP_STRUCT->Kernel_Set_Proprietary_Tag_qPBOC)

#define Kernel_load_callback_Routine_Rupay_     (P_API_MEM_MAP_STRUCT->Kernel_load_callback_Routine_Rupay)
#define Kernel_Init_Rupay_                      (P_API_MEM_MAP_STRUCT->Kernel_Init_Rupay)
#define Kernel_Start_Rupay_                     (P_API_MEM_MAP_STRUCT->Kernel_Start_Rupay)
#define Kernel_Customized_Contactless_Rupay_    (P_API_MEM_MAP_STRUCT->Kernel_Customized_Contactless_Rupay)
#define Kernel_Set_Proprietary_Tag_Rupay_       (P_API_MEM_MAP_STRUCT->Kernel_Set_Proprietary_Tag_Rupay)

#define Kernel_load_callback_Routine_MirPS_     (P_API_MEM_MAP_STRUCT->Kernel_load_callback_Routine_MirPS)
#define Kernel_Init_MirPS_                      (P_API_MEM_MAP_STRUCT->Kernel_Init_MirPS)
#define Kernel_Start_MirPS_                     (P_API_MEM_MAP_STRUCT->Kernel_Start_MirPS)
#define Kernel_Customized_Contactless_MirPS_    (P_API_MEM_MAP_STRUCT->Kernel_Customized_Contactless_MirPS)
#define Kernel_Set_Proprietary_Tag_MirPS_       (P_API_MEM_MAP_STRUCT->Kernel_Set_Proprietary_Tag_MirPS)

#define Kernel_load_callback_Routine_Interac_   (P_API_MEM_MAP_STRUCT->Kernel_load_callback_Routine_Interac)
#define Kernel_Init_Interac_                    (P_API_MEM_MAP_STRUCT->Kernel_Init_Interac)
#define Kernel_Start_Interac_                   (P_API_MEM_MAP_STRUCT->Kernel_Start_Interac)
#define Kernel_Customized_Contactless_Interac_  (P_API_MEM_MAP_STRUCT->Kernel_Customized_Contactless_Interac)
#define Kernel_Set_Proprietary_Tag_Interac_     (P_API_MEM_MAP_STRUCT->Kernel_Set_Proprietary_Tag_Interac)

#define Kernel_load_callback_Routine_PURE_      (P_API_MEM_MAP_STRUCT->Kernel_load_callback_Routine_PURE)
#define Kernel_Init_PURE_                       (P_API_MEM_MAP_STRUCT->Kernel_Init_PURE)
#define Kernel_Start_PURE_                      (P_API_MEM_MAP_STRUCT->Kernel_Start_PURE)
#define Kernel_Customized_Contactless_PURE_     (P_API_MEM_MAP_STRUCT->Kernel_Customized_Contactless_PURE)
#define Kernel_Set_Proprietary_Tag_PURE_        (P_API_MEM_MAP_STRUCT->Kernel_Set_Proprietary_Tag_PURE)

#define Kernel_load_callback_Routine_BANCOMAT_  (P_API_MEM_MAP_STRUCT->Kernel_load_callback_Routine_BANCOMAT)
#define Kernel_Init_BANCOMAT_                   (P_API_MEM_MAP_STRUCT->Kernel_Init_BANCOMAT)
#define Kernel_Start_BANCOMAT_                  (P_API_MEM_MAP_STRUCT->Kernel_Start_BANCOMAT)
#define Kernel_Customized_Contactless_BANCOMAT_ (P_API_MEM_MAP_STRUCT->Kernel_Customized_Contactless_BANCOMAT)
#define Kernel_Set_Proprietary_Tag_BANCOMAT_    (P_API_MEM_MAP_STRUCT->Kernel_Set_Proprietary_Tag_BANCOMAT)

#define Kernel_Init_Scan_                       (P_API_MEM_MAP_STRUCT->Kernel_Init_Scan)
#define Kernel_Set_Scan_                        (P_API_MEM_MAP_STRUCT->Kernel_Set_Scan)
#define Kernel_Get_Scan_                        (P_API_MEM_MAP_STRUCT->Kernel_Get_Scan)
#define Kernel_Decode_Scan_                     (P_API_MEM_MAP_STRUCT->Kernel_Decode_Scan)
#define Kernel_Get_Version_Scan_                (P_API_MEM_MAP_STRUCT->Kernel_Get_Version_Scan)

#define libs_export_                            (P_API_MEM_MAP_STRUCT->libs_export)

//	add by zhoumin
#define uart_hook_set_							(P_API_MEM_MAP_STRUCT->uart_hook_set)
#define os_sleep_								(P_API_MEM_MAP_STRUCT->os_sleep)
#define os_get_systick_							(P_API_MEM_MAP_STRUCT->os_get_systick)
#define os_cpu_sr_save_							(P_API_MEM_MAP_STRUCT->OS_CPU_SR_Save)
#define os_cpu_sr_restore_						(P_API_MEM_MAP_STRUCT->OS_CPU_SR_Restore)
#define os_ticks_per_sec_						(P_API_MEM_MAP_STRUCT->os_ticks_per_sec)

#define sys_status_ (P_API_MEM_MAP_STRUCT->sys_status)

#define TLV_VAL(tlv, tag)   		tlv_get_v_(tlv_find_(tlv, tag))
#define TLV_LEN(tlv, tag)   		tlv_get_l_(tlv_find_(tlv, tag))
/*-----------------------------------------------------------------------------
|   Enumerations
+----------------------------------------------------------------------------*/
enum
{
	SCATTER_FUN_STATUS_SYS = 0,
	SCATTER_FUN_STATUS_APP_USER,
};

/*-----------------------------------------------------------------------------
|   Typedefs
+----------------------------------------------------------------------------*/
#ifndef size_t
typedef unsigned int size_t;
#endif

typedef u32 (*pCallback_Process_EntryPoint_Fun)(pu8 param,pu32 len);
typedef u32 (*pCallback_Process_EMV)(pu8 param,pu32 len);

typedef struct
{
	/*api list status*/
	u32 fun_status;
	/*lark api*/
	u32 (*(LARK_API))(pvoid Param );
	/*cbk*/
	void (*(cbk_task1))(pvoid pParam );
	void (*(cbk_task2))(pvoid pParam );
    void (*(cbk_task3))(pvoid pParam );
	void (*(cbk_com_inchar))(u32 Type,u8 c );
	void (*(cbk_com_ouchar))(u32 Type,u8 c );
	void (*(cbk_assert))(pu8 file,u32 line);
	void (*(cbk_destruct))(pvoid pParam );
	void (*(cbk_get_mag_key))(pu8 pMagKey,pu32 pMagKeyLen );
	void (*(cbk_systick_hook))(pvoid pParam );
	/*C*/
	int (*(Isalnum))(int c);
	int (*(Isalpha))(int c);
	int (*(Iscntrl))(int c);
	int (*(Isdigit))(int c);
	int (*(Isgraph))(int c);

	int (*(Islower))(int c);
	int (*(Isprint))(int c);
	int (*(Ispunct))(int c);
	int (*(Isspace))(int c);
	int (*(Isupper))(int c);
	int (*(Isxdigit))(int c);
	int (*(Tolower))(int c);
	int (*(Toupper))(int c);
	double (*(Acos))(double x);
	double (*(Asin))(double x);

	double (*(Atan))(double x);
	double (*(Atan2))(double x);
	double (*(Cos))(double x);
	double (*(Cosh))(double x);
	double (*(Sin))(double x);
	double (*(Sinh))(double x);
	double (*(Tanh))(double x);
	double (*(Exp))(double x);
	double (*(Frexp))(double x);
	double (*(Ldexp))(double x);

	double (*(Log))(double x);
	double (*(Log10))(double x);
	double (*(Modf))(double x);
	double (*(Pow))(double x);
	double (*(Sqrt))(double x);
	double (*(Ceil))(double x);
	double (*(Fabs))(double x);
	double (*(Floor))(double x);
	double (*(Fmod))(double x);

	int (*(Printf))(const char *format, ...);
	int (*(Sprintf))(char *str, const char *format, ...);
	int (*(Eprintf))(const char *format, ...);

	double (*(Atof))(const char *str);

	int (*(Atoi))(const char *str);
	long int (*(Atol))(const char *str);
	double (*(Strtod))(const char *str, char **endptr);
	long int (*(Strtol))(const char *str, char **endptr, int base);
	unsigned long int (*(Strtoul))(const char *str, char **endptr, int base);
	int (*(Abs))(int x);
	void * (*(Memchr))(const void *str, int c, size_t n);
	int (*(Memcmp))(const void *str1, const void *str2, size_t n);
	void * (*(Memcpy))(void *dest, const void *src, size_t n);
	void * (*(Memmove))(void *dest, const void *src, size_t n);

	void * (*(Memset))(void *str, int c, size_t n);
	char * (*(Strcat))(char *dest, const char *src);
	char * (*(Strncat))(char *dest, const char *src, size_t n);
	char * (*(Strchr))(const char *str, int c);
	int (*(Strcmp))(const char *str1, const char *str2);
	int (*(Strncmp))(const char *str1, const char *str2, size_t n);
	int (*(Strcoll))(const char *str1, const char *str2);
	char * (*(Strcpy))(char *dest, const char *src);
	char * (*(Strncpy))(char *dest, const char *src, size_t n);
	size_t (*(Strcspn))(const char *str1, const char *str2);

	char * (*(Strerror))(int errnum);
	size_t (*(Strlen))(const char *str);
	char * (*(Strpbrk))(const char *str1, const char *str2);
	char * (*(Strrchr))(const char *str, int c);
	size_t (*(Strspn))(const char *str1, const char *str2);
	char * (*(Strstr))(const char *haystack, const char *needle);
	char * (*(Strtok))(char *str, const char *delim);
	size_t (*(Strxfrm))(char *dest, const char *src, size_t n);
	/*extended*/
	u32 (*(tlv_new))(pu8 pTlvData,u32 T,u32 L,pu8 pV );
	u32 (*(tlv_get_t))(pu8 pTlvData );

	u32 (*(tlv_get_t_len))(pu8 pTlvData );
	u32 (*(tlv_get_l))(pu8 pTlvData );
	u32 (*(tlv_get_l_len))(pu8 pTlvData );
	pu8 (*(tlv_get_v))(pu8 pTlvData );
	u32 (*(tlv_get_node_l))(pu8 pTlvData );
	u32 (*(tlv_add_child))(pu8 pTlvData,u32 T,u32 L,pu8 pV );
	pu8 (*(tlv_find))(pu8 pTlvData,u32 T );
	u32 (*(tlv_delete))(pu8 pTlvData,u32 T );
	u32 (*(tlv_modify))(pu8 pTlvData,u32 T,u32 L,pu8 pV );
    u32 (*(tlv_replace))(pu8 pTlvData, u32 T, u32 L, pu8 pV );

    u32 (*(tlv_move))(pu8 pTlvData, pu8 pTlvDstData, u32 T );
    bool (*(tlv_is_composite))(pu8 pTlvData);
    pu8 (*(tlv_get_first_child))(pu8 pTlvData);
    pu8 (*(tlv_get_next_child))(pu8 pTlvData);
    u32 (*(tlv_batch_move))(pu8 pTlvData, pu8 pTlvDstData);
	u32 (*(xor_nbytes))(pu8 pInSrc,pu8 pInOutSrc,u32 xorLen );

	/*kernel entry*/

	//emv
	u32 (*(Kernel_load_callback_Routine_EMV))(pCallback_Process_EMV pszRecvData,u32 dwRecvDataLen);
	u32 (*(Kernel_Init_EMV))(pu8 tlv_buffer,u32 len);
	u32 (*(Kernel_Prepare_AID_List_EMV))(pu8 tlv_buffer,u32 len);
	u32 (*(Kernel_Start_EMV))(pu8 tlv_buffer,pu32 len);
	u32 (*(Kernel_EMV_Get_ICCard_TransLog))(pu8 tlv_buffer,pu32 len);
	u32 (*(Kernel_EMV_Set_Element))(pu8 pInTLVData,u32 uiInTLVDataLen);
	u32 (*(Kernel_EMV_Get_Element))(u32 uiTAG,pu8 pOutData,pu32 pOutDataLen);
	u32 (*(Kernel_Customized_Contact_EMV))(pu8 buffer);

	//paypass
	u32 (*(Kernel_load_callback_Routine_PayPass))(pCallback_Process_EntryPoint_Fun pszRecvData,u32 dwRecvDataLen);
	u32 (*(Kernel_Init_PayPass))(pu8 tlv_buffer,u32 len);
	u32 (*(Kernel_Start_PayPass))(pu8 tlv_buffer,pu32 len);
	u32 (*(Kernel_Customized_Contactless_PayPass))(pvoid buffer);
	u32 (*(Kernel_Set_Proprietary_Tag_PayPass))(pu8 buffer);

	//paywave
	u32 (*(Kernel_load_callback_Routine_PayWave))(pCallback_Process_EntryPoint_Fun pszRecvData,u32 dwRecvDataLen);
	u32 (*(Kernel_Init_PayWave))(pu8 tlv_buffer,u32 len);
	u32 (*(Kernel_Start_PayWave))(pu8 tlv_buffer,pu32 len);
	u32 (*(Kernel_Customized_Contactless_PayWave))(pvoid buffer);
	u32 (*(Kernel_Set_Proprietary_Tag_PayWave))(pu8 buffer);

	//Amex
	u32 (*(Kernel_load_callback_Routine_AMEX))(pCallback_Process_EntryPoint_Fun pszRecvData, u32 dwRecvDataLen);
	u32 (*(Kernel_Init_AMEX))(pu8 tlv_buffer, u32 len);
	u32 (*(Kernel_Start_AMEX))(pu8 tlv_buffer, pu32 len);
	u32 (*(Kernel_Customized_Contactless_AMEX))(pvoid buffer);
	u32 (*(Kernel_Set_Proprietary_Tag_AMEX))(pu8 buffer);

	//JCB
	u32 (*(Kernel_load_callback_Routine_JCB))(pCallback_Process_EntryPoint_Fun pszRecvData, u32 dwRecvDataLen);
	u32 (*(Kernel_Init_JCB))(pu8 tlv_buffer, u32 len);
	u32 (*(Kernel_Start_JCB))(pu8 tlv_buffer, pu32 len);
	u32 (*(Kernel_Customized_Contactless_JCB))(pvoid buffer);
	u32 (*(Kernel_Set_Proprietary_Tag_JCB))(pu8 buffer);

	//Discover
	u32 (*(Kernel_load_callback_Routine_Discover))(pCallback_Process_EntryPoint_Fun pszRecvData, u32 dwRecvDataLen);
	u32 (*(Kernel_Init_Discover))(pu8 tlv_buffer,u32 len);
	u32 (*(Kernel_Start_Discover))(pu8 tlv_buffer, pu32 len);
	u32 (*(Kernel_Customized_Contactless_Discover))(pvoid buffer);
	u32 (*(Kernel_Set_Proprietary_Tag_Discover))(pu8 buffer);

	//qUICS
	u32 (*(Kernel_load_callback_Routine_qUICS))(pCallback_Process_EntryPoint_Fun pszRecvData,u32 dwRecvDataLen);
	u32 (*(Kernel_Init_qUICS))(pu8 tlv_buffer,u32 len);
	u32 (*(Kernel_Start_qUICS))(pu8 tlv_buffer,pu32 len);
	u32 (*(Kernel_Customized_Contactless_qUICS))(pvoid buffer);
	u32 (*(Kernel_Set_Proprietary_Tag_qUICS))(pu8 buffer);

	//qpboc
	u32 (*(Kernel_load_callback_Routine_qPBOC))(pCallback_Process_EntryPoint_Fun pszRecvData,u32 dwRecvDataLen);
	u32 (*(Kernel_Init_qPBOC))(pu8 tlv_buffer,u32 len);
	u32 (*(Kernel_Start_qPBOC))(pu8 tlv_buffer,pu32 len);
	u32 (*(Kernel_Customized_Contactless_qPBOC))(pvoid buffer);
	u32 (*(Kernel_Set_Proprietary_Tag_qPBOC))(pu8 buffer);

	//Rupay
	u32 (*(Kernel_load_callback_Routine_Rupay))(pCallback_Process_EntryPoint_Fun pszRecvData, u32 dwRecvDataLen);
	u32 (*(Kernel_Init_Rupay))(pu8 tlv_buffer, u32 len);
	u32 (*(Kernel_Start_Rupay))(pu8 tlv_buffer, pu32 len);
	u32 (*(Kernel_Customized_Contactless_Rupay))(pvoid buffer);
	u32 (*(Kernel_Set_Proprietary_Tag_Rupay))(pu8 buffer);

	//MirPS
	u32 (*(Kernel_load_callback_Routine_MirPS))(pCallback_Process_EntryPoint_Fun pszRecvData, u32 dwRecvDataLen);
	u32 (*(Kernel_Init_MirPS))(pu8 tlv_buffer, u32 len);
	u32 (*(Kernel_Start_MirPS))(pu8 tlv_buffer, pu32 len);
	u32 (*(Kernel_Customized_Contactless_MirPS))(pvoid buffer);
	u32 (*(Kernel_Set_Proprietary_Tag_MirPS))(pu8 buffer);

	//Interac
	u32 (*(Kernel_load_callback_Routine_Interac))(pCallback_Process_EntryPoint_Fun pszRecvData, u32 dwRecvDataLen);
	u32 (*(Kernel_Init_Interac))(pu8 tlv_buffer, u32 len);
	u32 (*(Kernel_Start_Interac))(pu8 tlv_buffer, pu32 len);
	u32 (*(Kernel_Customized_Contactless_Interac))(pvoid buffer);
	u32 (*(Kernel_Set_Proprietary_Tag_Interac))(pu8 buffer);

	//PURE
	u32 (*(Kernel_load_callback_Routine_PURE))(pCallback_Process_EntryPoint_Fun pszRecvData, u32 dwRecvDataLen);
	u32 (*(Kernel_Init_PURE))(pu8 tlv_buffer, u32 len);
	u32 (*(Kernel_Start_PURE))(pu8 tlv_buffer, pu32 len);
	u32 (*(Kernel_Customized_Contactless_PURE))(pvoid buffer);
	u32 (*(Kernel_Set_Proprietary_Tag_PURE))(pu8 buffer);

	//BANCOMAT
	u32 (*(Kernel_load_callback_Routine_BANCOMAT))(pCallback_Process_EntryPoint_Fun pszRecvData, u32 dwRecvDataLen);
	u32 (*(Kernel_Init_BANCOMAT))(pu8 tlv_buffer, u32 len);
	u32 (*(Kernel_Start_BANCOMAT))(pu8 tlv_buffer, pu32 len);
	u32 (*(Kernel_Customized_Contactless_BANCOMAT))(pvoid buffer);
	u32 (*(Kernel_Set_Proprietary_Tag_BANCOMAT))(pu8 buffer);

    //SCAN 扫码
    int (*(Kernel_Init_Scan))(void);
    int (*(Kernel_Set_Scan))(int key, int value);
    int (*(Kernel_Get_Scan))(int key, int *pvalue);
    int (*(Kernel_Decode_Scan))(void *bardata);
    const char * (*(Kernel_Get_Version_Scan))(void);

	/*third_party_librarys*/
	void* (*(libs_export))(const char* lib_name, const char* api_name);
	
	//	add by zhoumin
	void (*uart_hook_set)(int port, void *hook);
	void (*os_sleep)(unsigned int ticks);
	unsigned int (*os_get_systick)(void);
	unsigned int (*OS_CPU_SR_Save)(void);
	void (*OS_CPU_SR_Restore)(unsigned int sr);
	unsigned int os_ticks_per_sec;

	u32 sys_status;
}tAPIMemMap,*pAPIMemMap;

typedef struct _param_scatter_file_t
{
	u8     addr[4];
	u8     size[4];
	u8     crc_result[4];
	u8     fix[4];
} param_scatter_file_t, *pparam_scatter_file_t;

typedef struct _param_scatter_t
{
	param_scatter_file_t sys;
	param_scatter_file_t app_user[63];
} param_scatter_t, *pparam_scatter_t;

/*-----------------------------------------------------------------------------
|   Variables
+----------------------------------------------------------------------------*/
#if defined(VM_WIN32)
#if defined(CFG_APP_VERSION)
pAPIMemMap pAPIMemMapS;
#else
extern pAPIMemMap pAPIMemMapS;
#endif
#else
extern pAPIMemMap pAPIMemMapS;
#endif


/*-----------------------------------------------------------------------------
|   Constants
+----------------------------------------------------------------------------*/



/*-----------------------------------------------------------------------------
|   prototypes
+----------------------------------------------------------------------------*/

/*
*******************************************************************************
*   End of File
*******************************************************************************
*/

#ifdef __cplusplus
}
#endif//__cplusplus

#endif  /* #ifndef _SCATTER_SYS_H */



#include "appcode.h"
#include "lark_api.h"
#include "proj_sdk.h"
#include "pkt_adapt.h"
#include "interface.h"


T_SOCKET_INTERFACE s_current_sock = SOCK_NONE;

int obtain_sock_type(void)
{
	return s_current_sock;
}

char *trim(char *s, char cExt)
{
    char * p = s;
    int l = Strlen_(p);

    while(IS_SPACE(p[l - 1], cExt)) p[--l] = 0;
    while(* p && IS_SPACE(*p, cExt)) ++p, --l;
    Memcpy_(s, p, l + 1);
    return s;
}

u32 str2num(char *strAmt, u32 dwLen)
{
    u32 dwAmt = 0, dwFct = 1;
    int i;
    for(i = dwLen - 1; i >= 0; i--)
    {
        if(strAmt[i] >= '0' && strAmt[i] <= '9')
        {
            dwAmt += (strAmt[i] - '0')*dwFct;
            dwFct *= 10;
        }
    }
    return dwAmt;
}

char *uv2str(char *strRet, T_U8_VIEW *uv)
{
	if(strRet && uv)
	{
		Memcpy_(strRet, uv->head, uv->len);
		strRet[uv->len] = 0;
    }
	return strRet;
}

int uvcmpstr(T_U8_VIEW *uv, char *str)
{

	u32 i;
	if(uv->len != Strlen_(str)) return -1;
	for(i = 0; i < uv->len; i++)
		if(uv->head[i] != str[i]) return -1;
	return 0;
}

u32 su2num(T_U8_VIEW *uv)
{
    u32 dwAmt = 0, dwFct = 1;
    int i;
    for(i = uv->len - 1; i >= 0; i--)
    {
        if(uv->head[i] >= '0' && uv->head[i] <= '9')
        {
            dwAmt += (uv->head[i] - '0')*dwFct;
            dwFct *= 10;
        }
    }
    return dwAmt;
}

char *str_in_uv(T_U8_VIEW uvSrc, char *sub)
{
	char *bp, *sp;
	u8 *pTail = uvSrc.head + uvSrc.len;
    if(!UV_OK(uvSrc) || !sub) return (char *)uvSrc.head;

    while(uvSrc.head < pTail)
    {
        bp = (char *)uvSrc.head;
        sp = sub;
        do
        {
            if(!*sp) return (char *)uvSrc.head;
        }while(*sp++ == *bp++);
        uvSrc.head++;
    }
    return NULL;
}
#if 0
u32 uv2bin(u8 *pBin, T_U8_VIEW uvAsc, T_PAD_DIRECTION pad)
{
	u8 *pCur = uvAsc.head, *pDest = pBin;
	if(!UV_OK(uvAsc)) return 0;
	if((uvAsc.len%2) && pad == PAD_0_LEFT)	//4 higher bits : 0
	{
		*pDest++ = CHAR2BIN(*pCur);
		pCur++;
	}

	while(pCur < (uvAsc.head + uvAsc.len))
	{
		*pDest = (CHAR2BIN(*pCur)<<4);
		pCur++;
		if(pCur >= uvAsc.head + uvAsc.len) break;
		*pDest |= CHAR2BIN(*pCur);
		pCur++; pDest++;
	}
	return (pDest - pBin);
}
#endif
T_URL_INFO* parse_url(T_URL_INFO* info, char* strUrl)
{
	T_U8_VIEW uvTmp = {(u8 *)strUrl, 0};
	char *pSuHead = strUrl;
    if (!info || !strUrl) return NULL;
	char *pCur = Strstr_(strUrl, "://");
	if(!pCur)
		info->eProtocol = TCP;
	else
	{
		uvTmp.len = pCur - strUrl;
		if(uvcmpstr(&uvTmp, "http") == 0)
			info->eProtocol = HTTP;
		else if(uvcmpstr(&uvTmp, "https") == 0)
			info->eProtocol = HTTPS;
		else
			info->eProtocol = UNKNOWN_PROTO;
		pSuHead = pCur + 3;
	}
	pCur = Strstr_(pSuHead, ":");
	if(pCur)
	{
		info->uvSite.head = (u8 *)pSuHead;
		info->uvSite.len = pCur - pSuHead;
		pSuHead = pCur + 1;
	}
	pCur = Strstr_(pSuHead, "/");
	if(pCur)
	{
		if(info->uvSite.head)
		{
			uvTmp.head = (u8 *)pSuHead;

			uvTmp.len = pCur - pSuHead;
			info->wPort = su2num(&uvTmp);
		}
		else
		{
			info->uvSite.head = (u8 *)pSuHead;
			info->uvSite.len = pCur - pSuHead;
			if(info->eProtocol == HTTP)
				info->wPort = 80;
			else if(info->eProtocol == HTTPS)
				info->wPort = 443;
		}
		pSuHead = pCur + 1;
		info->uvPath.head = (u8 *)pSuHead;
		info->uvPath.len = Strlen_(pSuHead);
	}
	else if(info->eProtocol == TCP)
	{
		uvTmp.head = (u8 *)pSuHead;
		uvTmp.len = Strlen_(pSuHead);
		info->wPort = su2num(&uvTmp);
	}
	return info;
}

#if 0
static u32 detectHttpLen(T_U8_VIEW uvBlk)
{
	const char *strFixed = "Content-Length:";
	if(PTR2DWORD(uvBlk.head) != CHAR2LONG('H','T','T','P'))
	{
		TRACE_INFO(TMS_TRACE_LVL, uvBlk.head, 4, "%c", "HTTP head NOT found: ", 0);
		TRACE_INFO(TMS_TRACE_LVL, uvBlk.head, 4, "%02X", " -- ");
		return 0;
	}
	u8 *pCur = (u8 *)str_in_uv(uvBlk, "\r\n\r\n");
	if(!pCur)
	{
		TRACE_INFO(TMS_TRACE_LVL, uvBlk.head, uvBlk.len, "%c", "HTTP head_end NOT found: ");
		return 0;
	}
	T_U8_VIEW uvHead = {uvBlk.head, pCur + 4 - uvBlk.head};
	pCur = (u8 *)str_in_uv(uvBlk, (char *)strFixed);
	if(!pCur)
	{
		TRACE_INFO(TMS_TRACE_LVL, uvBlk.head, uvBlk.len, "%c", "Content-Length NOT found: \r\n");
		return 0;
	}
	pCur += Strlen_(strFixed);
	while(*pCur == ' ') pCur++;
	T_U8_VIEW uvNum = {pCur, 0};
	while(*pCur != '\r' && *pCur != '\n') pCur++;
	uvNum.len = pCur - uvNum.head;
	u32 dwLen = str2num((char *)uvNum.head, uvNum.len);
	TRACE(TMS_TRACE_LVL, "Content-Length = %d, head = %d, ", dwLen, uvHead.len);
	return (dwLen + uvHead.len);
}

Rc_t close_wifi_sock(void)
{
	esp8266_cts(1);
    if (esp8266_net_disconnect() != 0) {
		TRACE(TRACE_LVL_DEFAULT,"%s Failure\r\n",__FUNCTION__);
		return RC_FAIL;
    } else {
		TRACE(TRACE_LVL_DEFAULT,"%s Success\r\n",__FUNCTION__);
		return RC_SUCCESS;
	}
}

Rc_t close_cell_sock(void)
{
	sim_a7600_cts(1);
    if (sim_a7600_net_disconnect() != 0) {
		TRACE(TRACE_LVL_DEFAULT,"%s Failure\r\n",__FUNCTION__);
		return RC_FAIL;
    } else {
		TRACE(TRACE_LVL_DEFAULT,"%s Success\r\n",__FUNCTION__);
		return RC_SUCCESS;
	}
}

void close_mdm_sock(void)
{
	switch(s_current_sock)
	{
		case WIFI_SOCK:
			close_wifi_sock();
		break;
		case CELL_SOCK:
			close_cell_sock();
		break;
		default:
			TRACE(TMS_TRACE_LVL, "<%s>No socket found to close......\r\n", __FUNCTION__);
		break;
	}
}

static u32 handle_disconn_pattern(T_U8_VIEW uvData)
{
	u32 dwLenPat = sizeof(CELL_SOCK_DIS_PATTERN);
	if(uvData.len < dwLenPat) return uvData.len;
	if(!Memcmp_(uvData.head + uvData.len - dwLenPat, CELL_SOCK_DIS_PATTERN, dwLenPat))
	{
		TRACE(TMS_TRACE_LVL, "<%s>+++++++++++++++ Found disconn pattern! +++++++++++++++\r\n", __FUNCTION__);
		return dwLenPat;
	}
	return 0;
}


const char *SZ_MODEM[] = {"WiFi", "Cell", "NO NET"};
#define mdm_open(x)						(!s_current_sock ? esp8266_open(x) : sim_a7600_open(x));
#define mdm_cts(x) 						(!s_current_sock ? esp8266_cts(x) : sim_a7600_cts(x))
#define mdm_net_info_link() 			(!s_current_sock ? esp8266_net_info_link() : sim_a7600_net_info_link())
#define mdm_net_connect(a, b, c, d)		(!s_current_sock ? esp8266_net_connect(a, b, c, d) : sim_a7600_net_connect(a, b, c, d))
#define mdm_net_write(a, b, c, d)		(!s_current_sock ? esp8266_net_write(a, b, c, d) : sim_a7600_net_write(a, b, c, d))
#define mdm_net_read(a, b, c, d)		(!s_current_sock ? esp8266_net_read(a, b, c, d) : sim_a7600_net_read(a, b, c, d))

int modem_sock(T_URL_INFO *pUrlInfo, T_U8_VIEW uvSend, T_U8_VIEW uvRecv)
{
	int rc;
	if(s_current_sock == SOCK_NONE) return 0;
	mdm_cts(1);

	if(!mdm_net_info_link())
	{
		//disp_net_destination_ip();
		TRACE(TMS_TRACE_LVL, "Try to connect host via %s...\r\n", SZ_MODEM[s_current_sock]);
		char ip[80];
		Memcpy_(ip, pUrlInfo->uvSite.head, pUrlInfo->uvSite.len);
		ip[pUrlInfo->uvSite.len] = '\0';
		rc = mdm_net_connect(GPRS_CONN_TCP, ip, pUrlInfo->wPort, 15000);
		if (rc != 0) {
			TRACE(TMS_TRACE_LVL, "%s connect host failure, rc %d\r\n", SZ_MODEM[s_current_sock], rc);
			return RC_FAIL;
		} else {
			TRACE(TMS_TRACE_LVL, "%s connect host success\r\n", SZ_MODEM[s_current_sock]);
		}
	}

	if(UV_OK(uvSend))	//send
    {
		rc = mdm_net_write(0, uvSend.head, uvSend.len, 5000);
		if (rc < 0) {
			return RC_FAIL;
		}
		TRACE(TMS_TRACE_LVL, "net write %d\r\n", rc);
    }
	//s_current_sock = CELL_SOCK;

	if(!UV_OK(uvRecv))
	{
		TRACE(TMS_TRACE_LVL, "<%s>Req was sent to server, EXIT without waiting ...\r\n", __FUNCTION__);
		return 0;
	}

	u32 dwTimeOut = CELL_TIMEOUT_SEC*1000/EVENT_TIMEOUT_MS;
	T_U8_VIEW uvRecvPerWait = {uvRecv.head, 0};
	u32 dwLenProtocol = 0, dwLenGot = 0;
	if(uvSend.len) TRACE(TMS_TRACE_LVL, "<%s>Req of len %u was sent to server, waiting for response...\r\n", __FUNCTION__, uvSend.len);
    while(dwTimeOut)
    {
		int iLenEmpty = uvRecv.head + uvRecv.len - uvRecvPerWait.head;
		if(iLenEmpty <= 0)
		{
			TRACE(TMS_TRACE_LVL, "Too many data got from CELL(+%u)\r\n", uvRecvPerWait.len);
			return NET_ERROR_GPRS_FRAME_LENGTH;
		}

		int iRet = mdm_net_read(0, uvRecvPerWait.head, iLenEmpty, EVENT_TIMEOUT_MS);
		if(iRet < 0)
		{
			TRACE(TMS_TRACE_LVL, "<%s>mdm_net_read ret %d, exit...\r\n", __FUNCTION__, iRet);
			return iRet;
		}
		else if(!iRet)
		{
			dwTimeOut--;
			if(!(dwTimeOut%100)) TRACE(TMS_TRACE_LVL, "waiting for response...(%u)\r\n", dwTimeOut);
			continue;
		}
		//Got data
		int iLenDiscard = (s_current_sock == WIFI_SOCK ? 0 : handle_disconn_pattern((T_U8_VIEW){uvRecvPerWait.head, iRet}));
		uvRecvPerWait.len += iRet - iLenDiscard;
		if(!dwLenGot) dwTimeOut = PACKET_TIMEOUT_MS/EVENT_TIMEOUT_MS;
		dwLenGot +=  uvRecvPerWait.len;
		if(!UV_OK(uvSend))	dwLenProtocol = dwLenGot;
		TRACE(TMS_TRACE_LVL, "Got frame of %d bytes, %u in total, protocol len: %u\r\n", uvRecvPerWait.len, dwLenGot, dwLenProtocol);
		if(!dwLenProtocol)	//protocol length not yet detected
		{
			T_U8_VIEW uvDataGot = {uvRecv.head, dwLenGot};
			switch(pUrlInfo->eProtocol)
			{
				case HTTP:
				case HTTPS:
					dwLenProtocol = detectHttpLen(uvDataGot);
				break;
				case TCP:
					dwLenProtocol = (uvDataGot.head[0]<<8|uvDataGot.head[1]);
				break;
				default:
				break;
			}
			TRACE(TMS_TRACE_LVL, "perwait: %d,  total pac parsed: %d\r\n", uvDataGot.len, dwLenProtocol);
			if(!dwLenProtocol) TRACE(TMS_TRACE_LVL, "FAILED to detect HTTP frame, Got frame too short? wait more packet...\r\n");
		}
		if(iLenDiscard || (dwLenProtocol && (dwLenGot >= dwLenProtocol || (dwLenProtocol > uvRecv.len && strstr((char *)uvRecv.head, "\r\n\r\n")))))
		{
			TRACE(TMS_TRACE_LVL, "Got total len %d >= detected: %d, or sock closed, END HTTP resp!\r\n", dwLenGot, dwLenProtocol);
			break;
		}
		uvRecvPerWait.head += uvRecvPerWait.len;
		uvRecvPerWait.len = 0;
    }
	if(!dwTimeOut) TRACE(DBG_TRACE_LVL, "<%s>Timeout waiting for data...(recv: %u bytes)\r\n", __FUNCTION__, dwLenGot);
	mdm_cts(0);
	return dwLenGot;
}

int detect_modem(void)
{
	if( lark_flag_is_true(OS_FLAG_GROUP_SYS, SYS_EVENT_WIFI_AP_STATUS))
		s_current_sock = WIFI_SOCK;
    else if(Get_dev_sim_status())
		s_current_sock = CELL_SOCK;
	else
	{
		s_current_sock = SOCK_NONE;
		return -100;
	}
	return s_current_sock;
}

int modem_open(void)
{
	return mdm_open(MDM_PORT_BAUD_RATE);
}

void debug_uart_info(void)
{
	if(s_current_sock != SOCK_NONE)
		uart_info(s_current_sock == WIFI_SOCK ? 4 : 2);
}

int modem_close(void)
{
	switch(s_current_sock)
	{
		case WIFI_SOCK:
		return esp8266_close();
		case CELL_SOCK:
		return sim_a7600_close();
		default:
		break;
	}
	return 0;
}
#endif


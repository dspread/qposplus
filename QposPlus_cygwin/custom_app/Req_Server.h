/*
********************************************************************************
*
*   File Name:
*       Dev_Server.h
*   Author:
*       SW R&D Department
*   Version:
*       V1.0
*   Description:
*
*
********************************************************************************
*/

#ifndef _DEV_SERVER_H
#define _DEV_SERVER_H

/*-----------------------------------------------------------------------------
|   Includes
+----------------------------------------------------------------------------*/

#include "appcode.h"
#define URL_SERVER_PORT             (443)
#define ONLINE_REQUEST_TIMEOUT      (100000)
#define ONLINE_PACKET_TIMEOUT       (3000)
#define LOCAL_FIRMWARE_ADD          (0)//flash start address to save firmware
#define ONLINE_SEND_DATA_LEN        (512)
#define CFG_NET_CHECK_TICK          (50)
#define TLV_PARAM_DATA_MAX          (512)
#define FIRMWARE_REQUEST_LENGTH     (3200)
#define SERVER_REQUEST_BODY_LENGTH  (512)
#define SERVER_REQUEST_LENGTH       (1024)
#define SERVER_RECV_LENGTH          (1024)
#define PARAM_TLV_HEAD              (0x7F10)
#define PARAM_LENGTH                (512)
#define CFG_FW_DOWNLOAD_LENGTH      (SERVER_RECV_LENGTH-400)//Firmware download content size each request
#define CFG_FW_VERFIY_LENGTH        (512)
#define CFG_FW_DIV_LEN              (1024)



/*-----------------------------------------------------------------------------
|   Enumerations
+----------------------------------------------------------------------------*/

/*tlv custom tag*/
typedef enum
{
    CUSTOM_TAG_START = APP_TAG_START,
    TMS_FW_CRC,
    TMS_FW_PATH,
    TMS_FW_MSG,
    TMS_DEV,
    TMS_REQ_URL,
    TMS_FW_LENGTH,
    TMS_UPDATE_OFFSET,
    TMS_CONFIG_HEART,
    TMS_CONFIG_UPDATE,
    TMS_CONFIG_REVISION,
    FOTA_VERSION,
	TMS_FW_BKPT_DATA
}Custom_tag_t;


/*-----------------------------------------------------------------------------
|   prototypes
+----------------------------------------------------------------------------*/
extern void Signature_Test();
extern Rc_t GetTmsToken(pu8 pTlv);
extern Rc_t ServerReqFirmware(pu8 pTlv);
extern Rc_t req_patch_wifi(pu8 pTlv);
extern void LoadVersion(pu8 pTlv);
extern Rc_t CheckVersion();
#endif


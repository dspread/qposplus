#include "proj_sdk.h"
#include "lark_api.h"
#include "scatter.h"
#include "tms_ota.h"
#include "Req_Http.h"
#include "Req_Server.h"
#include "interface.h"

/*-----------------------------------------------------------------------------
|   Includes
+----------------------------------------------------------------------------*/


/*-----------------------------------------------------------------------------
|   Macros
+----------------------------------------------------------------------------*/
static void DisplayPercentage(u32 offset,u32 dwLenFile)
{
    char strDisp[20] = {0};
    u64 llRate = (u64)offset*10000/(u64)dwLenFile;
    u32 dwHiRate = offset*100/dwLenFile;
    u32 dwLoRate = llRate - dwHiRate*100;
    Sprintf_(strDisp, "%lu.%02d%%", dwHiRate, dwLoRate);
    LcdClearLine(LCD_NORMAL,LCD_LINE_0);
    LcdPrint(LCD_NORMAL, LCD_LINE_0, LCD_CENTERED_ALIGNMENT, CH_HEIGHT, strDisp);
}
void TmsProcess(u32 offset,u32 Length)
{
    u32 i=0;
    u32 cnt=0;
    u32 Percentage=0;
    DisplayPercentage(offset,Length);
    cnt = offset*100/Length;
    Percentage=cnt*2;
    if(offset==Length)
    {
        Percentage=PROGRESS_SIZE_X;
    }
    for(i=0;i<Percentage;i++)
    {
        LcdDrawLine(LCD_NORMAL, PROGRESS_START_X + i, PROGRESS_START_Y, PROGRESS_START_X + i, PROGRESS_START_Y - PROGRESS_SIZE_Y);
    }

}
void Progress_Init(const char * pTitle)
{
    LcdClearAll(LCD_NORMAL);
    LcdPrint(LCD_NORMAL, LCD_LINE_0, LCD_CENTERED_ALIGNMENT, CH_HEIGHT, pTitle);
    LcdDrawLine(LCD_NORMAL, PROGRESS_START_X, PROGRESS_START_Y, PROGRESS_START_X + PROGRESS_SIZE_X, PROGRESS_START_Y);
    LcdDrawLine(LCD_NORMAL, PROGRESS_START_X, PROGRESS_START_Y - PROGRESS_SIZE_Y, PROGRESS_START_X + PROGRESS_SIZE_X, PROGRESS_START_Y - PROGRESS_SIZE_Y);
    LcdDrawLine(LCD_NORMAL, PROGRESS_START_X, PROGRESS_START_Y, PROGRESS_START_X, PROGRESS_START_Y - PROGRESS_SIZE_Y);
    LcdDrawLine(LCD_NORMAL, PROGRESS_START_X + PROGRESS_SIZE_X, PROGRESS_START_Y, PROGRESS_START_X + PROGRESS_SIZE_X, PROGRESS_START_Y - PROGRESS_SIZE_Y);
}

void UpdateModuleProcess(u32 Percentage)
{
    u32 i=0;
    u8 strDisp[8]={0};
    disp_backlight();
    Sprintf_(strDisp,"%d%",Percentage);
    LcdClearLine(LCD_NORMAL,LCD_LINE_0);
    LcdPrint(LCD_NORMAL, LCD_LINE_0, LCD_CENTERED_ALIGNMENT, CH_HEIGHT, strDisp);
    for(i=0;i<Percentage*2;i++)
    {
        LcdDrawLine(LCD_NORMAL, PROGRESS_START_X + i, PROGRESS_START_Y, PROGRESS_START_X + i, PROGRESS_START_Y - PROGRESS_SIZE_Y);
    }
}
/*--------------------------------------
|   Function Name:
|       UpdateFotaProgress
|   Description:
|   Parameters:
|   Returns:
+-------------------------------------*/
void UpdateFotaProgress(u32 progress)
{
     UpdateModuleProcess(progress);
}
/*--------------------------------------
|   Function Name:
|       QposUpdate
|   Description:module update interface
|   Parameters:
|   Returns:
+-------------------------------------*/
Rc_t QposUpdate()
{
    Rc_t Rc=RC_FAIL;
    u32 idx=0;
    u32 len=Strlen_(CFG_OTA_URL_HEAD)+Strlen_(CFG_MODEL_FIRMWARE);

    if(Capabilities(CAP_FOTA)==RC_FAIL)
    {
        return Rc;
    }
    pu8 pTlvBuffer=lark_alloc_mem(CFG_TLV_POOL);
    tlv_new_(pTlvBuffer, PARAM_TLV_HEAD, 0, NULL);
    LoadVersion(pTlvBuffer);
    lark_free_mem(pTlvBuffer);
    Progress_Init(PROMPT_MODULE_OTA);
    T_UPDATE firmware={FW_CELLULAR,lark_alloc_mem(256)};
    pu8 pvalue=firmware.pvalue;

    pvalue[idx++]=0x01;
    pvalue[idx++]=LONG_LH(len);
    pvalue[idx++]=LONG_LL(len);
    Memcpy_(&pvalue[idx],CFG_OTA_URL_HEAD,Strlen_(CFG_OTA_URL_HEAD));
    idx+=Strlen_(CFG_OTA_URL_HEAD);
    Memcpy_(&pvalue[idx],CFG_MODEL_FIRMWARE,Strlen_(CFG_MODEL_FIRMWARE));
    idx+=Strlen_(CFG_MODEL_FIRMWARE);

    #if defined(CFG_OTA_DEBUG)
    TRACE(DBG_TRACE_LVL,"module url\r\n");
    TRACE_VALUE(DBG_TRACE_LVL,pvalue,idx);
    #endif

    Rc=FirmwareOTA(firmware);
    lark_free_mem(firmware.pvalue);

    #if defined(CFG_OTA_DEBUG)
    TRACE(DBG_TRACE_LVL,"<%s>-%s\r\n",__FUNCTION__,(Rc==RC_SUCCESS?"update success":"update fail"));
    #endif
    if(Rc==RC_SUCCESS)
    {
        LcdPrint(LCD_NORMAL, LCD_LINE_2,LCD_CENTERED_ALIGNMENT, CH_HEIGHT,"update success\nVersion Verify...");
        if(CheckVersion()==RC_SUCCESS)
        {
            dispStrWait(CENTER, PROMPT_MODULE_UPDATE_SUCCESS,3);
        }
    }
    return Rc;
}

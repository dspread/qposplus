/*
********************************************************************************
*
*   File Name:
*       Req_Server.c
*   Author:
*       SW R&D Department
*   Version:
*       V1.0
*   Description:
*
*
********************************************************************************
*/

/*-----------------------------------------------------------------------------
|   Includes
+----------------------------------------------------------------------------*/
#include "Req_Http.h"
#include "Req_Server.h"
#include "appcode.h"
#include "proj_sdk.h"
#include "lark_api.h"
#include "interface.h"
//#include "iso8583.h"
#include "pkt_adapt.h"
#include "cJSON.h"
#include "cjson_porting.h"

#if 1
#define URL_TEST    "http://spoc.dspread.com:8084/port/test"
#define SERVER_PORT  (8084)
#else
#define URL_TEST "https://www.baidu.com/port/test"
#define SERVER_PORT  (443)
#endif
#define TMS_PORT            9090
extern Rc_t RequestSelect(void);
extern void DispHttpRequst(void);
/*-----------------------------------------------------------------------------
|   Variables
+----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
|   Constants
+----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
|   Functions
+----------------------------------------------------------------------------*/
extern u32 crc32bzip2(u32 crc, void const *mem, size_t len);
/* {
    unsigned char const *data = mem;
	unsigned k;
    if (data == NULL) return 0;
    crc = ~crc;
    while (len--)
    {
        crc ^= (unsigned)(*data++) << 24;
        for (k = 0; k < 8; k++)
            crc = crc & 0x80000000 ? (crc << 1) ^ 0x4c11db7 : crc << 1;
    }
    crc = ~crc;
    return crc;
} */
/*--------------------------------------
|   Function Name:
|       RequestPackHttpFrame
|   Description:
|   Parameters:
|   Returns:
+-------------------------------------*/
Rc_t RequestPackHttpFrame(char * strMsg, char * pURL, char * pMethod,char * pFrame,char * pRange, u32 FrameLengthMax )
{
    HttpPack_t HttpPack;

    Memset_((pu8)(&HttpPack), 0x00, sizeof(HttpPack));
    TRACE(DBG_TRACE_LVL, "\r\nRequest_URL: %s\r\n\r\n",pURL);
    HttpPack.pRequestMessage = strMsg;
    HttpPack.pRequestBody = pFrame;
    HttpPack.Line.pMethod = lark_alloc_mem(32);
    HttpPack.Line.pUrl = lark_alloc_mem(256);
    HttpPack.Line.pVersion = lark_alloc_mem(32);
    HttpPack.Head.pConnection = lark_alloc_mem(32);
    HttpPack.Head.pHost = lark_alloc_mem(128);
    HttpPack.Head.pContentType = lark_alloc_mem(64);
    if(pRange!=NULL)
    {
        HttpPack.Head.pRange = lark_alloc_mem(64);
        Memset_(HttpPack.Head.pRange, 0x00, 64);
    }

    Memset_(HttpPack.pRequestBody, 0x00, FrameLengthMax);
    Memset_(HttpPack.Line.pMethod, 0x00, 32);
    Memset_(HttpPack.Line.pUrl, 0x00, 256);
    Memset_(HttpPack.Line.pVersion, 0x00, 32);
    Memset_(HttpPack.Head.pConnection, 0x00, 32);
    Memset_(HttpPack.Head.pHost, 0x00, 128);
    Memset_(HttpPack.Head.pContentType, 0x00, 64);


    Strcat_(HttpPack.Line.pMethod, pMethod);

    HttpUrlGetPath(pURL, HttpPack.Line.pUrl, 256);

    Strcat_(HttpPack.Line.pVersion, "HTTP/1.1");//HTTP/1.1

    Strcat_(HttpPack.Head.pConnection, "Keep-Alive");

    HttpUrlGetHost(pURL, HttpPack.Head.pHost, 128);

    if(pRange!=NULL)
    {
        Strcat_(HttpPack.Head.pRange, pRange);
    }

    Strcat_(HttpPack.Head.pContentType, "application/x-www-form-urlencoded");//application/json

    HttpFramePack(&HttpPack);

    lark_free_mem(HttpPack.Line.pMethod);
    lark_free_mem(HttpPack.Line.pUrl);
    lark_free_mem(HttpPack.Line.pVersion);
    lark_free_mem(HttpPack.Head.pConnection);
    lark_free_mem(HttpPack.Head.pHost);
    lark_free_mem(HttpPack.Head.pContentType);
    if(HttpPack.Head.pRange!=NULL)
    {
        lark_free_mem(HttpPack.Head.pRange);
    }

    return RC_SUCCESS;
}
/*--------------------------------------
|   Function Name:
|       GetHttpHeadFramLen
|   Description:
|   Parameters:
|   Returns:
+-------------------------------------*/
static u32 GetHttpHeadFramLen(const char *strMsg)
{
    int Cnt = 0;
    char* pStr=NULL;
    if(strMsg==NULL)
    {
        return Cnt;
    }
    pStr = Strstr_(strMsg, "\r\n\r\n");
    if(pStr)
    {
        pStr += 4;
/*        while(*pStr=='\r' || *pStr=='\n')*/
/*        {*/
/*            pStr++;*/
/*        }*/
        Cnt=pStr-strMsg;
    }
    else
    {
        Cnt = 0;
    }
    pStr=NULL;
    return Cnt;
}
/*--------------------------------------
|   Function Name:
|       GetHttpContentFram
|   Description:
|   Parameters:
|   Returns:
+-------------------------------------*/
static u32 GetHttpContentFram(const char *strMsg,s32 FramLen)
{
    s32 Cnt = 0;
    char* pStr=NULL;

    if(strMsg==NULL || FramLen==0)
    {
        return Cnt;
    }
    pStr = Strstr_(strMsg, "\r\n\r\n");
    if(pStr)
    {
        pStr += 4;

        Cnt=pStr-strMsg;
    }
    else
    {
        Cnt = 0;
    }
    return FramLen-Cnt;
}
/*--------------------------------------
|   Function Name:
|       RequestHttpMessageCheck
|   Description:
|   Parameters:
|   Returns:
+-------------------------------------*/
static bool RequestHttpMessageCheck(char * pHttpBody,u32 ReceiveLen)
{
    HttpUnpack_t HttpUnpack;
    u32 ContentLen = 0;
    u32 HeadLen=0;
    bool res=false;
    u8  pContentLen[32]={0};
    u8  pConnection[32]={0};
    if(pHttpBody==NULL || ReceiveLen==0)
    {
        return true;
    }
    TRACE(DBG_TRACE_LVL,"%s(%d)\r\n",__FUNCTION__,ReceiveLen);
    TRACE_VALUE(DBG_TRACE_LVL,pHttpBody,ReceiveLen);
    if(!Strstr_(pHttpBody,"\r\n\r\n"))
    {
        return false;
    }
    Memset_((pu8)(&HttpUnpack), 0x00, sizeof(HttpUnpack));
    HttpUnpack.pResponseBody = pHttpBody;

    HttpUnpack.Head.pContentLength = pContentLen;
    HttpUnpack.Head.pConnection = pConnection;
    Memset_(HttpUnpack.Head.pContentLength, 0x00,32);
    Memset_(HttpUnpack.Head.pConnection, 0x00,32);
    HttpFrameUnpack(&HttpUnpack);

    ContentLen = Atoi_(HttpUnpack.Head.pContentLength);
    if(ContentLen!=0)
    {
        HeadLen=GetHttpHeadFramLen(HttpUnpack.pResponseBody);
        #if defined(CFG_OTA_DEBUG)
        TRACE(DBG_TRACE_LVL,"\r\n%s--ReceiveLen=%d contentlen=%d  HeadLen=%d\r\n",__FUNCTION__,ReceiveLen,ContentLen,HeadLen);
        #endif
        if(ContentLen+HeadLen==ReceiveLen)
        {
            res= true;
        }
    }
    else if(Strcmp_(HttpUnpack.Head.pConnection,"close"))
    {
        res=true;
    }

    return res;
}
/*--------------------------------------
|   Function Name:
|       ParamFormatCheck
|   Description:
|   Parameters:
|   Returns:
+-------------------------------------*/
static void ParamFormatCheck(pu8 pTLV )
{
    if(tlv_get_t_(pTLV) != PARAM_TLV_HEAD)
    {
        Memset_(pTLV, 0x00, PARAM_LENGTH);
        tlv_new_(pTLV, PARAM_TLV_HEAD, 0, NULL);
        CustomParametersSet(pTLV, PARAM_LENGTH);
    }
}

/*--------------------------------------
|   Function Name:
|       MosParamTLV_GetData
|   Description:
|   Parameters:
|   Returns:
+-------------------------------------*/
static u32 ParamTLV_GetData(u32 Tag, pu8 pData, u32 LengthMax )
{
    pu8 pTLV_Buffer = NULL;
    pu8 pNode = NULL;
    pu8 pValue = NULL;
    u32 Length = 0;

    pTLV_Buffer = lark_alloc_mem(512);
    CustomParametersGet(pTLV_Buffer, 512);
    ParamFormatCheck(pTLV_Buffer);
    pNode = tlv_find_(pTLV_Buffer, Tag);
    if(pNode)
    {
        pValue = tlv_get_v_(pNode);
        Length = tlv_get_l_(pNode);
    }
    if(Length != 0 && Length <= LengthMax)
    {
        Memcpy_(pData, pValue, Length);
    }
    else
    {
        Length = 0;
    }
    lark_free_mem(pTLV_Buffer);
    return Length;
}
/*--------------------------------------
|   Function Name:
|       ParamTLV_SetData
|   Description:
|   Parameters:
|   Returns:
+-------------------------------------*/
static u32 ParamTLV_SetData(u32 Tag, pu8 pData, u32 Length )
{
    pu8 pTLV_Buffer = NULL;

    pTLV_Buffer = lark_alloc_mem(512);
    CustomParametersGet(pTLV_Buffer, 512);
    ParamFormatCheck(pTLV_Buffer);
    tlv_replace_(pTLV_Buffer, Tag, Length, pData);
    CustomParametersSet(pTLV_Buffer, 512);
    lark_free_mem(pTLV_Buffer);

    return RC_SUCCESS;
}

/*--------------------------------------
|   Function Name:
|       TradeLocalParametersLoad
|   Description:
|   Parameters:
|   Returns:
+-------------------------------------*/
Rc_t LocalParametersLoad(pu8 pTransBuffer, u32 Tag)
{
    pu8 pBuffer = NULL;
    u32 BufferLength = 0;
    Rc_t Rc=RC_FAIL;

    pBuffer = lark_alloc_mem(256);
    Memset_(pBuffer, 0x00, 256);
    BufferLength = ParamTLV_GetData(Tag, pBuffer, 256);
    if(BufferLength)
    {
        tlv_replace_(pTransBuffer, Tag, BufferLength, pBuffer);
        Rc=RC_SUCCESS;
    }
    lark_free_mem(pBuffer);
    return Rc;
}
Rc_t LocalParameterDelete(u32 Tag)
{
    pu8 pTLV_Buffer = NULL;
    pu8 pNode = NULL;
    pu8 pValue = NULL;
    u32 Length = 0;

    pTLV_Buffer = lark_alloc_mem(512);
    CustomParametersGet(pTLV_Buffer, 512);
    ParamFormatCheck(pTLV_Buffer);
    pNode = tlv_find_(pTLV_Buffer, PARAM_TLV_HEAD);
    if(pNode)
    {
        pValue=lark_alloc_mem(512);
        tlv_new_(pValue,0x7F00,tlv_get_l_(pNode),tlv_get_v_(pNode));
        if(tlv_find_(pValue,Tag))
        {
            tlv_delete_(pValue,Tag);
        }
        Memset_(pTLV_Buffer,0x00,512);
        tlv_new_(pTLV_Buffer,PARAM_TLV_HEAD,0,NULL);
        tlv_batch_move_(pValue,pTLV_Buffer);
        lark_free_mem(pValue);
        CustomParametersSet(pTLV_Buffer,512);
    }
    lark_free_mem(pTLV_Buffer);
}
/*--------------------------------------
|   Function Name:
|       TradeLocalParametersSave
|   Description:
|   Parameters:
|   Returns:
+-------------------------------------*/
Rc_t LocalParametersSave(pu8 pTransBuffer, u32 Tag)
{
    pu8 pNode = NULL;
    Rc_t Rc=RC_SUCCESS;

    pNode = tlv_find_(pTransBuffer, Tag);
    if(!pNode || tlv_get_l_(pNode) == 0)
    {
        Rc=RC_FAIL;
    }
    else
    {
        Rc=ParamTLV_SetData(Tag, tlv_get_v_(pNode), tlv_get_l_(pNode));
    }
    return Rc;
}

/*--------------------------------------
|   Function Name:
|       UpdateKeyDataToTLV
|   Description:
|   Parameters:
|   Returns:
+-------------------------------------*/
Rc_t UpdateKeyDataToTLV(char * strMsg, char * strKey, u32 Tag, pu8 pTradingFile )
{
    char * pTempBuffer = NULL;

    pTempBuffer = lark_alloc_mem(256);

    Memset_(pTempBuffer, 0x00, 256);

    HttpGetStrKey(strMsg, strKey, pTempBuffer, 256);

    if(Strlen_(pTempBuffer) == 0)
    {
        lark_free_mem(pTempBuffer);
        return RC_FAIL;
    }
    else
    {
        T_U8_VIEW uvValue={pTempBuffer,Strlen_(pTempBuffer)};
        //tlv_replace_(pTradingFile, Tag, Strlen_(pTempBuffer), (pu8)pTempBuffer);
        if(pTradingFile!=NULL)
        {
            set_tlv_view(pTradingFile,Tag, uvValue);
        }

        lark_free_mem(pTempBuffer);
        return RC_SUCCESS;
    }
}

/*--------------------------------------
|   Function Name:
|       KeyDataCompare
|   Description:
|   Parameters:
|   Returns:
+-------------------------------------*/
Rc_t KeyDataCompare(char * strMsg, char * strKey,const char * strCompareValue)
{
    char * pTempBuffer = NULL;

    pTempBuffer = lark_alloc_mem(256);

    Memset_(pTempBuffer, 0x00, 256);

    HttpGetStrKey(strMsg, strKey, pTempBuffer, 256);

    if(Strlen_(pTempBuffer) == 0)
    {
        lark_free_mem(pTempBuffer);
        return RC_FAIL;
    }

    if(Strcmp_(strCompareValue, pTempBuffer) != 0)
    {
        lark_free_mem(pTempBuffer);
        return RC_FAIL;
    }
    else
    {
        lark_free_mem(pTempBuffer);
        return RC_SUCCESS;
    }
}

/*--------------------------------------
|   Function Name:
|       HttpLineStatusCheck
|   Description:
|   Parameters:
|   Returns:
+-------------------------------------*/
static Rc_t HttpLineStatusCheck(pu8 pTradingFile, const char * pHttpBody)
{
    Rc_t Rc = RC_FAIL;
    HttpUnpack_t HttpUnpack;
    Memset_((pu8)(&HttpUnpack), 0x00, sizeof(HttpUnpack));
    HttpUnpack.pResponseBody = (char * )pHttpBody;

    HttpUnpack.Line.pStatus = lark_alloc_mem(64);
    Memset_(HttpUnpack.Line.pStatus, 0x00, 64);

    HttpFrameUnpack(&HttpUnpack);
    Rc=((Strcmp_(HttpUnpack.Line.pStatus,"200")|| Strcmp_(HttpUnpack.Line.pStatus,"206"))?RC_SUCCESS:RC_FAIL);
    lark_free_mem(HttpUnpack.Line.pStatus);
    return Rc;
}
/*--------------------------------------
|   Function Name:
|       HttpFrameTransmit
|   Description:
|   Parameters:
|   Returns:
+-------------------------------------*/
static s32 HttpFrameTransmit(pu8 pScheme, pu8 pHost, u32 Port, pu8 pData, u32 DataLength, pu8 pReceivedData, u32 ReceivedDataLengthMax, u32 Timeout )
{
    s32 ReceivedLength = 0;
    s32 Length = 0;

    if(pScheme==NULL || pHost==NULL || pData==NULL)
    {
        return 0;
    }
    if(Strcmp_((char *)pScheme, "https") == 0)
    {
        ReceivedLength = SslTransmit((pu8)pHost, Port, pData, DataLength, pReceivedData, ReceivedDataLengthMax, Timeout);
        if(ReceivedLength > 0)
        {
            if(RequestHttpMessageCheck((char * )pReceivedData,ReceivedLength))
            {
                ReceivedLength += Length;
                goto HttpFrameTransmit_end;
            }
            else
            {

            }
        }
        else if(ReceivedLength<0)
        {
            pReceivedData[0] = '\0';
            goto HttpFrameTransmit_end;
        }
        else
        {
            ReceivedLength = Length;
            pReceivedData[0] = '\0';
            goto HttpFrameTransmit_end;
        }

       while(1)
       {
           Length = SslTransmit((pu8)pHost, Port, NULL, 0, &pReceivedData[ReceivedLength], ReceivedDataLengthMax - ReceivedLength, ONLINE_PACKET_TIMEOUT);
           if(Length > 0)
           {
               ReceivedLength += Length;
               if(RequestHttpMessageCheck((char * )pReceivedData,ReceivedLength))
               {
                   goto HttpFrameTransmit_end;
               }
               else
               {
                   //continue to wait
               }
           }
           else
           {
               ReceivedLength = Length;
               pReceivedData[0] = '\0';
               goto HttpFrameTransmit_end;
           }
       }
   }
   else
   {
       ReceivedLength = SocketTransmit((pu8)pHost, Port, pData, DataLength, pReceivedData, ReceivedDataLengthMax, Timeout);
       if(ReceivedLength > 0)
       {
           if(RequestHttpMessageCheck((char * )pReceivedData,ReceivedLength))
           {
               ReceivedLength += Length;
               goto HttpFrameTransmit_end;
           }
           else
           {
               //continue to wait
           }
       }
       else if(ReceivedLength<0)
       {
             pReceivedData[0] = '\0';
             goto HttpFrameTransmit_end;
       }
       else
       {
           goto HttpFrameTransmit_end;
       }

       while(1)
       {
           Length = SocketTransmit((pu8)pHost, Port, NULL, 0, &pReceivedData[ReceivedLength], ReceivedDataLengthMax - ReceivedLength, ONLINE_PACKET_TIMEOUT);
           if(Length > 0)
           {
               ReceivedLength += Length;
               if(RequestHttpMessageCheck((char * )pReceivedData,ReceivedLength))
               {
                   goto HttpFrameTransmit_end;
               }
               else
               {
                   //continue to wait
               }
           }
           else
           {
               ReceivedLength = Length;
               pReceivedData[0] = '\0';
               goto HttpFrameTransmit_end;
           }
       }
   }

HttpFrameTransmit_end:
    if(ReceivedLength < 0)
    {
        TRACE(DBG_TRACE_LVL, "ErrCode =%d:\r\n",ReceivedLength);
    }
    return ReceivedLength;
}

/*--------------------------------------
|   Function Name:
|       RequestHttpTransmit
|   Description:
|   Parameters:
|   Returns:
+-------------------------------------*/
s32 RequestHttpTransmit(char * pSend,u32 SendLen, char * pURL,u32 port, char * pReceived, u32 FrameLengthMax )
{
    char * pScheme = NULL;
    char * pHost = NULL;
    s32    Length = 0;
    if(pSend == NULL || pURL == NULL || FrameLengthMax == 0)
    {
        return 0;
    }

    TRACE(DBG_TRACE_LVL, "\r\npSend(%d) : \r\n%s\r\n\r\n",SendLen,pSend);
    //TRACE_VALUE(DBG_TRACE_LVL,pSend,SendLen);

    pScheme = lark_alloc_mem(128);
    pHost = lark_alloc_mem(64);
    Memset_(pScheme, 0x00, 128);
    Memset_(pHost, 0x00,64);

    HttpUrlGetScheme(pURL, pScheme, 128);
    check_param(Strlen_(pScheme) <= 128);

    HttpUrlGetHost(pURL, pHost, 64);
    check_param(Strlen_(pHost) <= 64);
    //request config
    //AUTH_NONE:does not verify Cert
    //AUTH_SERVER_CLIENT:verify client and server cert
    //AUTH_SERVER:verify server cert only
    //AUTH_CLIENT:verify client cert only
    if(HttpServerConnect(pURL,port,pHost,AUTH_NONE)==RC_FAIL)//Test url does not verify cert by default
    {
        goto RequestHttpTransmit_end;
    }
    Length = HttpFrameTransmit((pu8 )pScheme, (pu8 )pHost, port, (pu8)(pSend),SendLen, (pu8)pReceived, FrameLengthMax, ONLINE_REQUEST_TIMEOUT);
    check_param(Strlen_(pReceived) <= FrameLengthMax);
RequestHttpTransmit_end:
    if(pScheme!=NULL)
    {
        Memset_(pScheme, 0x00, 128);
        lark_free_mem(pScheme);
        pScheme=NULL;
    }
    if(pHost!=NULL)
    {
        Memset_(pHost, 0x00,64);
        lark_free_mem(pHost);
        pHost=NULL;
    }
    TRACE(DBG_TRACE_LVL, "\r\nRECV(%d) : \r\n",Length);
    return Length;
}
void SetRequestMode(u32 index)
{
    if(index==0)
    {
        SetDevWifi(COMMON_WIFI);
    }
    else if(index==1)
    {
         SetDevWifi(NO_WIFI);
    }
}
/*--------------------------------------
|   Function Name:
|       lcd_sign
|   Description:
|   Parameters:
|   Returns:
+-------------------------------------*/
static Rc_t lcd_sign(void)
{
    Tp_Pos_t position;
    u16 lastpos[2] = {0xFFFF,0xFFFF};
    u32 size = 0;
    bool loop=true;
    Rc_t Rc=RC_FAIL;
    Key_Num_t keyValue;

    lark_flag_clr(OS_FLAG_GROUP_SYS, SYS_EVENT_TP_TOUCH);
    disp_backlight();
    while(loop)
    {
        if(lark_flag_is_true(OS_FLAG_GROUP_SYS, SYS_EVENT_TP_TOUCH))
        {
            lark_flag_clr(OS_FLAG_GROUP_SYS, SYS_EVENT_TP_TOUCH);
            lark_tp_read_map(&position);
            if((position.x>10)&&(position.x<70)&&(position.y>205)&&(position.y<235))//clr
            {
                if(position.event == TP_EVENT_UP)
                {
                    lark_lcd_sign_init(0, 0, 320, 200, 0xFFFFFF);
                }
                lastpos[0] = 0xFFFF;
                continue;
            }
            if((position.x>270)&&(position.x<310)&&(position.y>205)&&(position.y<235))//ok
            {
                if(position.event == TP_EVENT_UP)
                {
                    Rc=RC_SUCCESS;
            		break;
                }
                lastpos[0] = 0xFFFF;
                continue;
            }

            if(position.y>197)//比实际限值小一点，避免线条加宽处理时超出签名区
            {
                lastpos[0] = 0xFFFF;
                continue;
            }

            if(position.event != TP_EVENT_DOWN)
    		{
    			lastpos[0] = 0xFFFF;
    			continue;
    		}
            if(lastpos[0] == 0xFFFF)
    		{
    			lastpos[0] = position.x;
    			lastpos[1] = position.y;
    		}

            lark_lcd_sign_write(lastpos[0], lastpos[1], position.x, position.y, 2, 0x000000);
    		lastpos[0] = position.x;
    		lastpos[1] = position.y;
    		position.x = position.y = 0;
        }

        if(lark_flag_is_true(OS_FLAG_GROUP_SYS, SYS_EVENT_KBD_PRESSED))
        {
            lark_flag_clr(OS_FLAG_GROUP_SYS, SYS_EVENT_KBD_PRESSED);

            keyValue = lark_kbd_read_current();
            if(keyValue == KEY_CANCEL)
            {
                lark_lcd_sign_init(0, 0, 320, 200, 0xFFFFFF);
                lastpos[0] = 0xFFFF;
                continue;
            }
            else if(keyValue == KEY_CONFIRM)
            {
                Rc=RC_SUCCESS;
        		break;
            }
            else
            {}
        }
    }
    return Rc;
}

/*--------------------------------------
|   Function Name:
|       Signature_Test
|   Description:
|   Parameters:
|   Returns:
+-------------------------------------*/
void Signature_Test()
{
    Rc_t Rc=RC_FAIL;

    SignatureTransmit();
    lcd_sign_menu();
    Rc=lcd_sign();
    if(Rc==RC_SUCCESS)
    {
        pu8 pSign=lark_alloc_mem(2048);
        Memset_(pSign,0x00,2048);
        u32 len=SignatureCallBack(pSign,2048);
        #ifdef CFG_DBG
        TRACE(DBG_TRACE_LVL,"sign length(%d)\r\n",len);
        TRACE_VALUE(DBG_TRACE_LVL,pSign,len);
        #endif
        lark_lcd_sign_init(0, 200, 320,40, 0xFFFFFF);
        lark_free_mem(pSign);
    }

}
/*--------------------------------------
|   Function Name:
|       ServerReques_test
|   Description:
|   Parameters:
|   Returns:
+-------------------------------------*/
Rc_t ServerReques_test(void)
{
    Rc_t Rc=RC_SUCCESS;
    char * strmsg=NULL;
    char * pRecv=NULL;
    s32    RecvLen=0;
    u32    HeadLen=0;

    if(lark_flag_is_true(OS_FLAG_GROUP_SYS, SYS_EVENT_WIFI_AP_STATUS) && Get_dev_sim_status())
    {
        #ifdef CFG_REQUEST_MODE
        Rc=RequestSelect();
        #endif
        if(Rc!=RC_SUCCESS)
        {
            return Rc;
        }
    }
    strmsg=lark_alloc_mem(512);
    pRecv=lark_alloc_mem(1024);
    DispHttpRequst();
    RequestPackHttpFrame(NULL,URL_TEST,"POST",strmsg,NULL,512);
    RecvLen=RequestHttpTransmit(strmsg,Strlen_(strmsg),URL_TEST,SERVER_PORT,pRecv,1024);
    lark_free_mem(strmsg);
    #ifdef CFG_DBG
    TRACE(DBG_TRACE_LVL,"recv(%d)\r\n",RecvLen);
    #endif
    if(RecvLen>0)
    {
        TRACE_VALUE(DBG_TRACE_LVL,pRecv,RecvLen);
        HeadLen=GetHttpHeadFramLen((char*)pRecv);
        T_U8_VIEW Http_head={pRecv,HeadLen};
        T_U8_VIEW Http_Response={pRecv+HeadLen,RecvLen-HeadLen};
        #ifdef CFG_DBG
        TRACE(DBG_TRACE_LVL,"Http_head(%d)\r\n",Http_head.len);
        TRACE_ASCII(DBG_TRACE_LVL,Http_head.head,Http_head.len);
        TRACE(DBG_TRACE_LVL,"Response(%d)\r\n",Http_Response.len);
        TRACE_VALUE(DBG_TRACE_LVL,Http_Response.head,Http_Response.len);
        #endif

        if(HttpLineStatusCheck(NULL,Http_head.head)==RC_SUCCESS || Memcmp_(Http_Response.head,"hello",5)==0)
        {
            Rc=RC_SUCCESS;
            #if defined(CFG_SIGNATURE)
            if(Capabilities(CAP_TP)==RC_SUCCESS)
            {
                Signature_Test();//signature interface sample code
            }
            #endif
            #ifdef CFG_DBG
            TRACE(DBG_TRACE_LVL,"request success\r\n");
            #endif
        }
    }
    else
    {
        Rc=RC_FAIL;
    }
    lark_free_mem(pRecv);

    HttpServerDisConnect();
    return Rc;
}
/*--------------------------------------
|   Function Name:
|       ParsingTransResponse
|   Description:you need to impletment,
|           POS_TAG_RES_EN_ONLINE_RESULT:tag for transaction result,will transmit to card
|   Returns:
+-------------------------------------*/
static Rc_t ParsingTransResponse(pu8 pResponse,pu8 pTransBuffer)
{
    /*if get approve from server*/
    tlv_replace_(pTransBuffer,POS_TAG_RES_EN_ONLINE_RESULT, 4, (pu8)"\x8A\x02\x30\x30");
    return RC_SUCCESS;
}
/*--------------------------------------
|   Function Name:
|       CardTransaction_test
|   Description:it's sample code for card transaction online request,you need to modify
|                URL_TRANS_TEST:your transaction server URL
|   Returns:
+-------------------------------------*/
Rc_t CardTransaction_test(pu8 pTransBuffer)
{
    char * strmsg=NULL;
    char * pSend=NULL;
    char * pRecv=NULL;
    s32    RecvLen=0;
    u32    HeadLen=0;
    Rc_t Rc=RC_FAIL;
    /*step1:packet your request body*****/
    strmsg=lark_alloc_mem(SERVER_REQUEST_BODY_LENGTH);//SERVER_REQUEST_BODY_LENGTH:request body message length(can be modify according to needed)
    pSend=lark_alloc_mem(SERVER_REQUEST_LENGTH);//SERVER_REQUEST_LENGTH:request message length(request body lenth+request head length)
    //strmsg:request body in specific format(json or other format)
    //pSend:request message(include request body and request head)
    //RequestPackHttpFrame():you can modify request head according to needed

    //URL_TRANS_TEST:your server URL
    //SERVER_PORT:your server port
    RequestPackHttpFrame(strmsg,URL_TEST,"POST",pSend,NULL,SERVER_REQUEST_LENGTH);
    lark_free_mem(strmsg);
    /*step2:transmit request to server**/
    pRecv=lark_alloc_mem(SERVER_RECV_LENGTH);//SERVER_RECV_LENGTH:response length(can be modify according to needed)
    RecvLen=RequestHttpTransmit(pSend,Strlen_(pSend),URL_TEST,SERVER_PORT,pRecv,SERVER_RECV_LENGTH);
    lark_free_mem(pSend);
    /*step3:recv response and parsing**/
    if(RecvLen>0)
    {
        TRACE_VALUE(DBG_TRACE_LVL,pRecv,RecvLen);
        HeadLen=GetHttpHeadFramLen((char*)pRecv);
        T_U8_VIEW Http_head={pRecv,HeadLen};
        T_U8_VIEW Http_Response={pRecv+HeadLen,RecvLen-HeadLen};
        TRACE(DBG_TRACE_LVL,"Http_head(%d)\r\n",Http_head.len);
        TRACE_ASCII(DBG_TRACE_LVL,Http_head.head,Http_head.len);
        TRACE(DBG_TRACE_LVL,"Response(%d)\r\n",Http_Response.len);
        TRACE_VALUE(DBG_TRACE_LVL,Http_Response.head,Http_Response.len);
        if(HttpLineStatusCheck(NULL,Http_head.head)==RC_SUCCESS)
        {
            Rc=RC_SUCCESS;
            TRACE(DBG_TRACE_LVL,"request success\r\n");
            Rc=ParsingTransResponse(Http_Response.head,pTransBuffer);
        }
    }
    else
    {
        //pls find net error code in proj_sdk.h for detail
        TRACE(DBG_TRACE_LVL," Error:%d\r\n",RecvLen);
    }
    lark_free_mem(pRecv);
    HttpServerDisConnect();
    return Rc;
}
/*--------------------------------------
|   Function Name:
|       OTA_Upgrade
|   Description:application upgrader interface
|   Parameters:
|   Returns:
+-------------------------------------*/
void OTA_Upgrade()
{
    T_UPDATE Fw;
    T_OTA dev;
    dev.type=HOST_DEV_OTA;
    dev.start_addr=LOCAL_FIRMWARE_ADD;
    Fw.type=FW_APP;
    Fw.dev=dev;
    Fw.pvalue=NULL;
    while(!Get_dev_UsbConnect_Status())
    {
        LcdClearAll(LCD_NORMAL);
        LcdPrint(LCD_NORMAL,LCD_LINE_0,LCD_CENTERED_ALIGNMENT, CH_HEIGHT,"Please Connect USB\nUpdate...");
        WaitEvents(APP_EVENT_TIMEOUT,1000,NULL);
    }
    FirmwareOTA(Fw);
}

void LoadVersion(pu8 pTlv)
{
    if(LocalParametersLoad(pTlv,FOTA_VERSION)!=RC_SUCCESS)
    {
        pu8 pVer=lark_alloc_mem(64);
        u32 len=0;
        Memset_(pVer,0x00,64);
        len=GetCellularVersion(pVer);
        T_U8_VIEW Ver={pVer,len};
        set_tlv_view(pTlv,FOTA_VERSION,Ver);
        LocalParametersSave(pTlv,FOTA_VERSION);
        lark_free_mem(pVer);
    }
}
Rc_t CheckVersion()
{
    Rc_t Rc=RC_FAIL;
    u32 Event=0;

    pu8 pTlvBuffer=lark_alloc_mem(512);
    tlv_new_(pTlvBuffer, PARAM_TLV_HEAD, 0, NULL);

    if(LocalParametersLoad(pTlvBuffer,FOTA_VERSION)==RC_SUCCESS)
    {
        pu8 pVer=lark_alloc_mem(64);
        u32 len=0;
        T_U8_VIEW Ver=get_tlv_view(pTlvBuffer,FOTA_VERSION);
        if(UV_OK(Ver))
        {
            while(1)
            {
                Memset_(pVer,0x00,64);
                len=GetCellularVersion(pVer);
                if(Ver.len==len && Memcmp_(pVer,Ver.head,Ver.len)!=0)
                {
                    #if defined(CFG_OTA_DEBUG)
                    TRACE(DBG_TRACE_LVL,"update Fota ver[%s]-[%s]\r\n",Ver.head,pVer);
                    #endif
                    T_U8_VIEW Up_ver={pVer,len};
                    set_tlv_view(pTlvBuffer,FOTA_VERSION,Up_ver);
                    LocalParametersSave(pTlvBuffer,FOTA_VERSION);
                    Rc=RC_SUCCESS;
                    break;
                }
                Event=WaitEvents(APP_EVENT_TIMEOUT|APP_EVENT_USER_CANCEL,5000,NULL);
                if(Event==APP_EVENT_USER_CANCEL)
                {
                    break;
                }
            }
        }
        lark_free_mem(pVer);

    }
    lark_free_mem(pTlvBuffer);
    return Rc;
}
void req_balance(pu8 pTradingFile){
    char pan[32]={0};

    char* pNode = tlv_find_(pTradingFile, POS_TAG_RES_PAN);
    if(pNode){
        memcpy(pan,tlv_get_v_(pNode),tlv_get_l_(pNode));
    }


    Rc_t Rc=RC_SUCCESS;
    char * strmsg=NULL;
    char * pRecv=NULL;
    s32    RecvLen=0;
    u32    HeadLen=0;

    strmsg=lark_alloc_mem(512);
    pRecv=lark_alloc_mem(1024);
    DispHttpRequst();

    char format[]="{\"Pan\":\"%s\"}";

    sprintf(strmsg,format,pan);
    TRACE(0,"strmsg = %s\r\n",strmsg);

    RequestPackHttpFrame(NULL,URL_TEST,"POST",strmsg,NULL,512);
    RecvLen=RequestHttpTransmit(strmsg,Strlen_(strmsg),URL_TEST,SERVER_PORT,pRecv,1024);
    lark_free_mem(strmsg);
#ifdef CFG_DBG
    TRACE(DBG_TRACE_LVL,"recv(%d)\r\n",RecvLen);
#endif
    if(RecvLen>0)
    {
        TRACE_VALUE(DBG_TRACE_LVL,pRecv,RecvLen);
        HeadLen=GetHttpHeadFramLen((char*)pRecv);
        T_U8_VIEW Http_head={pRecv,HeadLen};
        T_U8_VIEW Http_Response={pRecv+HeadLen,RecvLen-HeadLen};
    #ifdef CFG_DBG
        TRACE(DBG_TRACE_LVL,"Http_head(%d)\r\n",Http_head.len);
        TRACE_ASCII(DBG_TRACE_LVL,Http_head.head,Http_head.len);
        TRACE(DBG_TRACE_LVL,"Response(%d)\r\n",Http_Response.len);
        TRACE_VALUE(DBG_TRACE_LVL,Http_Response.head,Http_Response.len);
    #endif

        if(HttpLineStatusCheck(NULL,Http_head.head)==RC_SUCCESS)
        {
            Rc=RC_SUCCESS;

        #ifdef CFG_DBG
            TRACE(DBG_TRACE_LVL,"request success\r\n");
        #endif
            //TODO display the balance
        }
    }
    else
    {
        Rc=RC_FAIL;
    }
    lark_free_mem(pRecv);
    return Rc;
}




void req_changePin(pu8 pTradingFile){
    Rc_t Rc=RC_SUCCESS;
    char * strmsg=NULL;
    char * pRecv=NULL;
    s32    RecvLen=0;
    u32    HeadLen=0;
    char newpin[24]={0};
    char oldpin[24]={0};
    char pan[32]={0};
    char * pSend=NULL;

    cJSON *json_root= NULL;
    //init json
    CjsonPortingInit();
    json_root =cJSON_CreateObject();
    //prepare pack data
    //add obj to json obj
    char *pNode = tlv_find_(pTradingFile, POS_TAG_ASC_NEW_PIN);

    if(pNode){
        Memcpy_(newpin,tlv_get_v_(pNode),tlv_get_l_(pNode));
        cJSON_AddStringToObject(json_root, "newPin", newpin);
    }
    pNode = tlv_find_(pTradingFile, POS_TAG_ASC_PIN);
    if(pNode){
        Memcpy_(oldpin,tlv_get_v_(pNode),tlv_get_l_(pNode));
        cJSON_AddStringToObject(json_root, "oldpin", oldpin);
    }
    pNode = tlv_find_(pTradingFile, POS_TAG_RES_PAN);
    if(pNode){
        Memcpy_(pan,tlv_get_v_(pNode),tlv_get_l_(pNode));
        cJSON_AddStringToObject(json_root, "pan", pan);
    }
    //TODO add your mac alg
    //1. get mac.
    //Eg: mac = Des(newpin+oldpin+pan);
    //2. add mac to json
    //Eg:cJSON_AddStringToObject(json_root, "mac", mac);
    //convert json obj to json string.
    strmsg = cJSON_PrintUnformatted(json_root);

    pSend = lark_alloc_mem(1024);
    pRecv=lark_alloc_mem(1024);
    Memset_(pSend,0,1024);
    Memset_(pRecv,0,1024);
    //here pack data to pSend.  add headers
    RequestPackHttpFrame(strmsg,URL_TEST,"POST",pSend,NULL,1024);

    //Note: cJSON_PrintUnformatted internal malloc buffer to save data. So remember to call lark_free_mem.
    lark_free_mem(strmsg);
    TRACE(DBG_TRACE_LVL,"post data = %s\r\n" ,pSend);

    RecvLen=RequestHttpTransmit(pSend,Strlen_(pSend),URL_TEST,SERVER_PORT,pRecv,1024);

    //Assuming the data returns as follows
    Memset_(pRecv,0,1024);
    Strcpy_(pRecv,"{\"respcode\":1,\"message\":\"Success\",\"balance\":\"1000.00\"}");
    RecvLen = Strlen_(pRecv);

    cJSON* cjson_Response = cJSON_Parse(pRecv);
    if(cjson_Response  == NULL){
        TRACE(DBG_TRACE_LVL,"cjson parse error, Plz check the data.\r\n" );
        goto End;
    }


    cJSON *jsonObj =NULL;
    jsonObj = cJSON_GetObjectItem(cjson_Response,"message");
    if(jsonObj){
        //one way
        char *msg =  cJSON_GetStringValue(jsonObj);
        TRACE(DBG_TRACE_LVL,"response msg = %s\r\n",msg);
        //another way
        if(jsonObj->type == cJSON_String){
            TRACE(DBG_TRACE_LVL,"response msg2 = %s\r\n",jsonObj->valuestring);
        }
    }

    jsonObj = cJSON_GetObjectItem(cjson_Response,"respcode");
    if(jsonObj){
        if(jsonObj->type == cJSON_Number){
            TRACE(DBG_TRACE_LVL,"response code = %d\r\n",jsonObj->valueint);
        }
    }

    jsonObj = cJSON_GetObjectItem(cjson_Response,"balance");
    if(jsonObj){
        //one way
        char *balance = cJSON_GetStringValue(jsonObj);
        TRACE(DBG_TRACE_LVL,"response balance = %s\r\n",balance);
        //another way
        if(jsonObj->type == cJSON_String){
            TRACE(DBG_TRACE_LVL,"response balance2 = %s\r\n",jsonObj->valuestring);
        }
    }


    End:
        if(pSend){
            lark_free_mem(pSend);
            pSend=NULL;
        }

        if(pRecv){
            lark_free_mem(pRecv);
            pRecv=NULL;
        }
        if(cjson_Response){
            cJSON_Delete(cjson_Response);
            cjson_Response = NULL;
        }

        //https mode
        if(Memcmp_((char *)URL_TEST, "https",5) == 0){
            SetTlsStatus(false);
            CommonNetTlsClose();
        }else{//http
            if(lark_flag_is_true(OS_FLAG_GROUP_SYS, SYS_EVENT_WIFI_AP_STATUS))
            {
                disconnect_wifi_des();
            }
            else
            {
                disconnect_Gprs_des();
            }
        }
        return Rc;
}
#ifdef JSCON_TEST
Rc_t core_request(pu8 pTradingFile)
{
    cJSON *json_test = NULL;
    pu8 strmsg = NULL;
    pu8 pSend = NULL;
    pu8 pRecv = NULL;
    s32 RecvLen = 0;
    u32 HeadLen = 0;
    u32 strsmsgLen = 0;
    Rc_t Rc = RC_FAIL;
    u8 url[128] = {0};
    u32 urlLen = 0;
    TRACE(DBG_TRACE_LVL, "%s--start\r\n", __FUNCTION__);

    pSend = lark_alloc_mem(2048);
    Memset_(pSend, 0, 2048);
    // --
    CjsonPortingInit();
    json_test = cJSON_CreateObject();

    pu8 tlvDataN = tlv_find_(pTradingFile, POS_TAG_RES_KN_ONLINE_DATA);
    if (tlvDataN)
    {
        // --
        pu8 serialBuffer = lark_alloc_mem(64);
        Memset_(serialBuffer, 0, 64);
        pu8 serialTBuffer = lark_alloc_mem(64);
        Memset_(serialTBuffer, 0, 64);

        GetPosid(serialBuffer);
        bcd_to_asc_sample(serialBuffer, 10, serialTBuffer);
        cJSON_AddStringToObject(json_test, "serial", serialTBuffer);
        lark_free_mem(serialBuffer);
        lark_free_mem(serialTBuffer);
        // --
        strmsg = cJSON_PrintUnformatted(json_test);
    }

    // --
    RequestPackHttpFrame(strmsg, URL_TEST, "POST", pSend, NULL, SERVER_REQUEST_LENGTH);
    lark_free_mem(strmsg);
    cJSON_Delete(json_test);

    pRecv = lark_alloc_mem(SERVER_RECV_LENGTH);
    Memset_(pRecv, 0, SERVER_RECV_LENGTH);
    RecvLen = RequestHttpTransmit(pSend, Strlen_(pSend), URL_TEST, SERVER_PORT, pRecv, SERVER_RECV_LENGTH);
    lark_free_mem(pSend);

    if (RecvLen > 0)
    {
        HeadLen = GetHttpHeadFramLen((char *)pRecv);
        T_U8_VIEW Http_head = {pRecv, HeadLen};
        T_U8_VIEW Http_Response = {pRecv + HeadLen, RecvLen - HeadLen};
        TRACE(DBG_TRACE_LVL, "Http_head(%d)\r\n", Http_head.len);
        TRACE_ASCII(DBG_TRACE_LVL, Http_head.head, Http_head.len);
        TRACE(DBG_TRACE_LVL, "Response(%d)\r\n", Http_Response.len);
        TRACE_ASCII(DBG_TRACE_LVL, Http_Response.head, Http_Response.len);
        if (HttpLineStatusCheck(pTradingFile, Http_head.head) == RC_SUCCESS)
        {
            TRACE(DBG_TRACE_LVL, "request success\r\n");
            if (Http_Response.len > 0)
            {
                ;
            }
            Rc = RC_SUCCESS;
        }
        else
        {
            tlv_replace_(pTradingFile, POS_TAG_RES_EN_ONLINE_RESULT, 4, (pu8) "\x8A\x02\x5A\x33");
        }
    }
    else
    {
        tlv_replace_(pTradingFile, POS_TAG_RES_EN_ONLINE_RESULT, 4, (pu8) "\x8A\x02\x5A\x33");
        Rc = RC_QUIT;
        TRACE(DBG_TRACE_LVL, "server connect fail\r\n");
    }

    lark_free_mem(pRecv);

    return Rc;
}
#endif
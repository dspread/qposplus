#ifndef _TMS_CFG_H
#define _TMS_CFG_H

/*-----------------------------------------------------------------------------
|   Includes
+----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
|   Macros
+----------------------------------------------------------------------------*/
#define TMS_FW_HEART                "http://cs.dspread.net:9090"
//#define TMS_FW_HEART_CUSTOM 	    "http://www.dspreadserv.net:9997"//亚马逊服务器
#define TMS_FW_UPDATE_ADDR          (0x1000)	//reserved 1 sector (4096B) for other use
#define TMS_SOCK_RETRY_CNT          (3)
/*-----------------------------------------------------------------------------
|   Typedefs
+----------------------------------------------------------------------------*/
typedef enum
{
    OTA_SYNC_SERVER=0,
    OTA_START_DOWNLOAD,
    OTA_PROCESSING,
    OTA_UPDATE_FINISH,
    OTA_OTHER,
    OTA_FIRMWARE_VERIFY,
    OTA_FIRMWARE_VERIFY_FAIL,
    OTA_SYNC_ERROR_MSG,
    OTA_SYNC_PARAM_DATA,
    OTA_NO_AVAILABLE_FIRMWARE,
	OTA_USB_CONNECT
}OTA_STEP;
#endif

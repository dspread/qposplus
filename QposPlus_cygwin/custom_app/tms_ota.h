#ifndef _TMS_OTA_H
#define _TMS_OTA_H

/*-----------------------------------------------------------------------------
|   Includes
+----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
|   Macros
+----------------------------------------------------------------------------*/
#define PROGRESS_START_X        (50)
#define PROGRESS_START_Y        (-60)
#define PROGRESS_SIZE_X         (200)
#define PROGRESS_SIZE_Y         (8)
#define CFG_TLV_POOL            (1024)


#define URL_TMS_HEART 	    "http://cs.dspread.net:9090/api/v1/heartbeat?"
#define TMS_CFG_HEART       "1440"
#define TMS_CFG_UPDATE      "800"
#define TMS_CFG_REVISIONS   "2a6d0191b"

#define CFG_OTA_URL_HEAD       "http://spoc.dspread.com:8088/download/4G/SIMCOM/7670E/"
#define CFG_MODEL_FIRMWARE     "SIMCOM_A7670E_B04V05_TO_B04V04_211105.bin"
//#define CFG_MODEL_FIRMWARE     "SIMCOM_A7670E_B04V04_TO_B04V05_211105.bin"

/*-----------------------------------------------------------------------------
|   Typedefs
+----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
|   prototypes
+----------------------------------------------------------------------------*/
extern void Progress_Init(const char * pTitle);
extern void SetProgress(u32 cnt,u32 offset,u32 conlen,u32 TotalLen);
extern Rc_t TmsFirmwareUpdate(bool Force);
extern Rc_t QposUpdate();
#endif
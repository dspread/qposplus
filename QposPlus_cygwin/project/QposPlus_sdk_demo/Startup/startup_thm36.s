;/**************************************************************************//**
; * @file     startup_thm36.s
; * @brief    Core Device Startup File for
; *           thm36x2 Device Series
; * @version  V5.00
; * @date     02. March 2018
; ******************************************************************************/


;/*
;//-------- <<< Use Configuration Wizard in Context Menu >>> ------------------
;*/


; <h> Stack Configuration
;   <o> Stack Size (in Bytes) <0x0-0xFFFFFFFF:8>
; </h>

Stack_Size      EQU     0x00000000

                AREA    STACK, NOINIT, READWRITE, ALIGN=3
Stack_Mem       SPACE   Stack_Size
__initial_sp


; <h> Heap Configuration
;   <o>  Heap Size (in Bytes) <0x0-0xFFFFFFFF:8>
; </h>

Heap_Size       EQU     0x00000000

                AREA    HEAP, NOINIT, READWRITE, ALIGN=3
__heap_base
Heap_Mem        SPACE   Heap_Size
__heap_limit


                PRESERVE8
                THUMB


; Vector Table Mapped to Address 0 at Reset

                AREA    RESET, DATA, READONLY
                EXPORT  __Vectors
                EXPORT  __Vectors_End
                EXPORT  __Vectors_Size

__Vectors       DCD     __initial_sp              ; Top of Stack
                DCD     Reset_Handler             ; Reset Handler
                DCD     NMI_Handler               ; NMI Handler
                DCD     HardFault_Handler         ; Hard Fault Handler
                DCD     MemManage_Handler         ; MPU Fault Handler
                DCD     BusFault_Handler          ; Bus Fault Handler
                DCD     UsageFault_Handler        ; Usage Fault Handler
                DCD     0                         ; Reserved
                DCD     0                         ; Reserved
                DCD     0                         ; Reserved
                DCD     0                         ; Reserved
                DCD     SVC_Handler               ; SVCall Handler
                DCD     DebugMon_Handler          ; Debug Monitor Handler
                DCD     0                         ; Reserved
                DCD     0            ; PendSV Handler
                DCD     0           ; SysTick Handler

                ; External Interrupts
; ToDo:  Add here the vectors for the device specific external interrupts handler
                DCD     0      	  ;  0: Default
				DCD		0         				  ;  1:
				DCD		0       				  ;  2:
				DCD		0      	  ;  3:
				DCD		0      	  ;  4:
				DCD		0      				  	  ;  5:
				DCD		0      				  	  ;  6:
				DCD		0      	  ;  7:
				DCD		0      				      ;  8:
				DCD		0      				      ;  9:
				DCD		0      	  ;  10:
				DCD		0      	  ;  11:
				DCD		0      	  ;  12:
				DCD		0      	  ;  13:
				DCD		0      				      ;  14:
				DCD		0		      ;  15:
				DCD		0		      ;  16:
				DCD		0      	  ;  17:
				DCD		0		      ;  18:
				DCD		0      	  ;  19:
				DCD		0	  ;  20:
				DCD		0      	  ;  21:
				DCD		0      	  ;  22:
				DCD 	0         ;  23:
				DCD		0      	  ;  24:
				DCD		0      	  ;  25:
				DCD		0      	  ;  26:
				DCD		0      	  ;  27:
				DCD		0      	  ;  28:
				DCD		0        				  ;  29:
				DCD		0      				      ;  30:
				DCD		0		      			  ;  31:
				DCD		0      				      ;  32: 0
				DCD		0     	  ;  33: 1
				DCD		0      				      ;  34: 2
				DCD		0      				      ;  35: 3
				DCD		0      	  ;  36: 4
				DCD		0      	  ;  37: 5
				DCD		0      	  				  ;  38: 6
				DCD		0      	  ;  39: 7
				DCD		0      	  ;  40: 8
				DCD		0      	  ;  41: 9
				DCD		0      	  ;  42: 10
				DCD		0                         ;  43: 11
				DCD		0      	  ;  44: 12
				DCD		0      ;  45: 13
				DCD		0      ;  46: 14
				DCD		0      					  ;  47: 15
				DCD		0      	  ;  48: 16
				DCD		0      	  ;  49: 17
				DCD		0      	  				  ;  50: 18
				DCD		0      	  ;  51: 19
				DCD		0      	  				  ;  52: 20
				DCD		0      	  ;  53: 21
				DCD		0       				  ;  54: 22
				DCD		0            			  ;  55: 23
				DCD		0      				      ;  56: 23
				DCD		0      	  				  ;  57: 23
				DCD		0      				      ;  58: 23
				DCD		0      				      ;  59: 23
				DCD		0      				      ;  60: 23
				DCD		0      				      ;  61: 23
				DCD		0      				      ;  62: 23
				DCD		0      				      ;  63: 23
				DCD		0       ;  64: 23
				DCD		0      ;  65: 23
				DCD		0      ;  66: 23
				DCD		0      ;  67: 23
				DCD		0      ;  68: 23
				DCD		0      	  ;  69: 23
				DCD		0      	  ;  70: 23
				DCD		0      	  ;  71: 23
				DCD		0      	  ;  72: 23
				DCD		0      	  ;  73: 23
				DCD		0      	  ;  74: 23
__Vectors_End

__Vectors_Size  EQU     __Vectors_End - __Vectors

                AREA    |.text|, CODE, READONLY


; Reset Handler

Reset_Handler   PROC
                EXPORT  Reset_Handler             [WEAK]
                IMPORT  __main
                LDR     R0, =__main
                BX      R0
                ENDP


; Dummy Exception Handlers (infinite loops which can be modified)

NMI_Handler     PROC
                EXPORT  NMI_Handler               [WEAK]
                B       .
                ENDP
HardFault_Handler\
                PROC
                EXPORT  HardFault_Handler         [WEAK]
                B       .
                ENDP
MemManage_Handler\
                PROC
                EXPORT  MemManage_Handler         [WEAK]
                B       .
                ENDP
BusFault_Handler\
                PROC
                EXPORT  BusFault_Handler          [WEAK]
                B       .
                ENDP
UsageFault_Handler\
                PROC
                EXPORT  UsageFault_Handler        [WEAK]
                B       .
                ENDP
SVC_Handler     PROC
                EXPORT  SVC_Handler               [WEAK]
                B       .
                ENDP
DebugMon_Handler\
                PROC
                EXPORT  DebugMon_Handler          [WEAK]
                B       .
                ENDP
PendSV_Handler  PROC
                EXPORT  PendSV_Handler            [WEAK]
                B       .
                ENDP
SysTick_Handler PROC
                EXPORT  SysTick_Handler           [WEAK]
                B       .
                ENDP


                ALIGN

; User Initial Stack & Heap

                IF      :DEF:__MICROLIB

                EXPORT  __initial_sp
                EXPORT  __heap_base
                EXPORT  __heap_limit

                ELSE

                IMPORT  __use_two_region_memory
                EXPORT  __user_initial_stackheap

__user_initial_stackheap PROC
                LDR     R0, =  Heap_Mem
                LDR     R1, =(Stack_Mem + Stack_Size)
                LDR     R2, = (Heap_Mem +  Heap_Size)
                LDR     R3, = Stack_Mem
                BX      LR
                ENDP

                ALIGN

                ENDIF
										

                END

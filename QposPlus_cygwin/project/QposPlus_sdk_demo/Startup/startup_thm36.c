
/*********************************************************************************
* @file     startup_thm36.c
* @brief    Cortex-M3 Device Startup File for THM36
* @version  V1.0.0
* @author   Feng Jian
* @date     18. March 2021
********************************************************************************
*/

/*----------------------------------------------------------------------------
  Exception / Interrupt Handler Function Prototype
 *----------------------------------------------------------------------------*/
typedef void(*pFunc)(void);
#define Reserved (pFunc)0

/*----------------------------------------------------------------------------
  External References
 *----------------------------------------------------------------------------*/
extern int		main(void);
extern unsigned long 	_etext;
extern unsigned long 	__data_start__;
extern unsigned long 	__data_end__;
extern unsigned long 	__bss_start__;
extern unsigned long 	__bss_end__;
extern unsigned long 	__StackTop;

/*----------------------------------------------------------------------------
  Internal References
 *----------------------------------------------------------------------------*/
void Default_Handler(void);
void Reset_Handler  (void);
void FSP_IRQHandler	(void);

/*----------------------------------------------------------------------------
  Exception / Interrupt Handler
 *----------------------------------------------------------------------------*/
/* Exceptions */
void NMI_Handler            (void) __attribute__ ((weak, alias("Default_Handler")));
void HardFault_Handler      (void) __attribute__ ((weak, alias("Default_Handler")));
void MemManage_Handler      (void) __attribute__ ((weak, alias("Default_Handler")));
void BusFault_Handler       (void) __attribute__ ((weak, alias("Default_Handler")));
void UsageFault_Handler     (void) __attribute__ ((weak, alias("Default_Handler")));
void SVC_Handler            (void) __attribute__ ((weak, alias("Default_Handler")));
void DebugMon_Handler       (void) __attribute__ ((weak, alias("Default_Handler")));
void PendSV_Handler         (void) __attribute__ ((weak, alias("Default_Handler")));
void SysTick_Handler        (void) __attribute__ ((weak, alias("Default_Handler")));
void RCC_IRQHandler			(void) __attribute__ ((weak, alias("Default_Handler")));
void DMA_IRQHandler			(void) __attribute__ ((weak, alias("Default_Handler")));
void AHBMMU_IRQHandler		(void) __attribute__ ((weak, alias("Default_Handler")));
void MPU_IRQHandler			(void) __attribute__ ((weak, alias("Default_Handler")));
void SM4_IRQHandler			(void) __attribute__ ((weak, alias("Default_Handler")));
void AES_IRQHandler			(void) __attribute__ ((weak, alias("Default_Handler")));
void DES_IRQHandler			(void) __attribute__ ((weak, alias("Default_Handler")));
void PKE_IRQHandler			(void) __attribute__ ((weak, alias("Default_Handler")));
void EGS_IRQHandler			(void) __attribute__ ((weak, alias("Default_Handler")));
void LD_IRQHandler			(void) __attribute__ ((weak, alias("Default_Handler")));
void TRNG_IRQHandler		(void) __attribute__ ((weak, alias("Default_Handler")));
void FD2_IRQHandler			(void) __attribute__ ((weak, alias("Default_Handler")));
void FD3_IRQHandler			(void) __attribute__ ((weak, alias("Default_Handler")));
void VB_SENSOR_IRQHandler	(void) __attribute__ ((weak, alias("Default_Handler")));
void ASH_IRQHandler			(void) __attribute__ ((weak, alias("Default_Handler")));
void WWDT_IRQHandler		(void) __attribute__ ((weak, alias("Default_Handler")));
void TIMER1_IRQHandler		(void) __attribute__ ((weak, alias("Default_Handler")));
void TIMER2_IRQHandler		(void) __attribute__ ((weak, alias("Default_Handler")));
void TIMER3_IRQHandler		(void) __attribute__ ((weak, alias("Default_Handler")));
void TIMER4_IRQHandler		(void) __attribute__ ((weak, alias("Default_Handler")));
void TIMER5_IRQHandler		(void) __attribute__ ((weak, alias("Default_Handler")));
void TIMER6_IRQHandler		(void) __attribute__ ((weak, alias("Default_Handler")));
void Flash1_IRQHandler		(void) __attribute__ ((weak, alias("Default_Handler")));
void GPIO_IRQHandler		(void) __attribute__ ((weak, alias("Default_Handler")));
void SPI1_IRQHandler		(void) __attribute__ ((weak, alias("Default_Handler")));
void SPI2_IRQHandler		(void) __attribute__ ((weak, alias("Default_Handler")));
void SPI3_IRQHandler		(void) __attribute__ ((weak, alias("Default_Handler")));
void UART1_IRQHandler		(void) __attribute__ ((weak, alias("Default_Handler")));
void UART2_IRQHandler		(void) __attribute__ ((weak, alias("Default_Handler")));
void UART3_IRQHandler		(void) __attribute__ ((weak, alias("Default_Handler")));
void UART4_IRQHandler		(void) __attribute__ ((weak, alias("Default_Handler")));
void ISO7816M1_IRQHandler	(void) __attribute__ ((weak, alias("Default_Handler")));
void ISO7816M2_IRQHandler	(void) __attribute__ ((weak, alias("Default_Handler")));
void ISO7816M3_IRQHandler	(void) __attribute__ ((weak, alias("Default_Handler")));
void PWM1_IRQHandler		(void) __attribute__ ((weak, alias("Default_Handler")));
void PWM2_IRQHandler		(void) __attribute__ ((weak, alias("Default_Handler")));
void DCMI_IRQHandler		(void) __attribute__ ((weak, alias("Default_Handler")));
void USB_IRQHandler			(void) __attribute__ ((weak, alias("Default_Handler")));
void ADC_IRQHandler			(void) __attribute__ ((weak, alias("Default_Handler")));
void ISO7816S_IRQHandler	(void) __attribute__ ((weak, alias("Default_Handler")));
void SWP_IRQHandler			(void) __attribute__ ((weak, alias("Default_Handler")));
void EXTI0_15_IRQHandler 	(void) __attribute__ ((weak, alias("Default_Handler")));
void EXTI16_31_IRQHandler	(void) __attribute__ ((weak, alias("Default_Handler")));
void EXTI32_47_IRQHandler	(void) __attribute__ ((weak, alias("Default_Handler")));
void EXTI48_63_IRQHandler	(void) __attribute__ ((weak, alias("Default_Handler")));
void EXTI64_79_IRQHandler	(void) __attribute__ ((weak, alias("Default_Handler")));
void EXTI80_IRQHandler		(void) __attribute__ ((weak, alias("Default_Handler")));
void EXTI81_IRQHandler		(void) __attribute__ ((weak, alias("Default_Handler")));
void EXTI82_IRQHandler		(void) __attribute__ ((weak, alias("Default_Handler")));
void EXTI83_IRQHandler		(void) __attribute__ ((weak, alias("Default_Handler")));
void EXTI84_IRQHandler		(void) __attribute__ ((weak, alias("Default_Handler")));
void EXTI85_IRQHandler		(void) __attribute__ ((weak, alias("Default_Handler")));

__attribute__ ((section(".isr_vector")))

pFunc VectorTable[] =
{
/*01*/	(pFunc)(&__StackTop),	//The initial stack pointer is the top of SRAM((pFunc)(0x20020000)), (pFunc)(&__StackTop)
/*02*/	Reset_Handler,        	//The reset handler 
/*03*/	NMI_Handler,
/*04*/	HardFault_Handler,
/*05*/	MemManage_Handler,
/*06*/	BusFault_Handler,
/*07*/	UsageFault_Handler,
/*08*/	Reserved,
/*09*/	Reserved,
/*10*/	Reserved,
/*11*/	Reserved,
/*12*/	SVC_Handler,
/*13*/	DebugMon_Handler,
/*14*/	Reserved,
/*15*/	PendSV_Handler,
/*16*/	SysTick_Handler,
//External Interrupts           External Interrupts
/*01*/	RCC_IRQHandler,
/*02*/	Reserved ,
/*03*/	Reserved ,
/*04*/	DMA_IRQHandler,
/*05*/	AHBMMU_IRQHandler,
/*06*/	Reserved,
/*07*/	Reserved,
/*08*/	MPU_IRQHandler,
/*09*/	Reserved,
/*10*/	Reserved,
/*11*/	SM4_IRQHandler,
/*12*/	AES_IRQHandler,
/*13*/	DES_IRQHandler,
/*14*/	PKE_IRQHandler,
/*15*/	Reserved,
/*16*/	EGS_IRQHandler,
/*17*/	LD_IRQHandler,
/*18*/	TRNG_IRQHandler,
/*19*/	FD2_IRQHandler,
/*20*/	FD3_IRQHandler,
/*21*/	VB_SENSOR_IRQHandler,
/*22*/	ASH_IRQHandler,
/*23*/	WWDT_IRQHandler,
/*24*/	TIMER1_IRQHandler,
/*25*/	TIMER2_IRQHandler,
/*26*/	TIMER3_IRQHandler,
/*27*/	TIMER4_IRQHandler,
/*28*/	TIMER5_IRQHandler,
/*29*/	TIMER6_IRQHandler,
/*30*/	Reserved,
/*31*/	Reserved,
/*32*/	Reserved,
/*33*/	Reserved,
/*34*/	Flash1_IRQHandler,
/*35*/	Reserved,
/*36*/	Reserved,
/*37*/	GPIO_IRQHandler,
/*38*/	SPI1_IRQHandler,
/*39*/	SPI2_IRQHandler,
/*40*/	SPI3_IRQHandler,
/*41*/	UART1_IRQHandler,
/*42*/	UART2_IRQHandler,
/*43*/	UART3_IRQHandler,
/*44*/	Reserved,
/*45*/	Reserved,
/*46*/	ISO7816M1_IRQHandler,
/*47*/	ISO7816M2_IRQHandler,
/*48*/	ISO7816M3_IRQHandler,
/*49*/	PWM1_IRQHandler,
/*50*/	PWM2_IRQHandler,
/*51*/	DCMI_IRQHandler,
/*52*/	USB_IRQHandler,
/*53*/	UART4_IRQHandler,
/*54*/	ADC_IRQHandler,
/*55*/	ISO7816S_IRQHandler,
/*56*/	SWP_IRQHandler,
/*57*/	Reserved,
/*58*/	FSP_IRQHandler,
/*59*/	Reserved,
/*60*/	Reserved,
/*61*/	Reserved,
/*62*/	Reserved,
/*63*/	Reserved,
/*64*/	Reserved,
/*65*/	EXTI0_15_IRQHandler ,
/*66*/	EXTI16_31_IRQHandler,
/*67*/	EXTI32_47_IRQHandler,
/*68*/	EXTI48_63_IRQHandler,
/*69*/	EXTI64_79_IRQHandler,
/*70*/	EXTI80_IRQHandler,
/*71*/	EXTI81_IRQHandler,
/*72*/	EXTI82_IRQHandler,
/*73*/	EXTI83_IRQHandler,
/*74*/	EXTI84_IRQHandler,
/*75*/	EXTI85_IRQHandler,
};

//***************************************************************************** 
// 
// The following are constructs created by the linker, indicating where the 
// the "data" and "bss" segments reside in memory.  The initializers for the 
// for the "data" segment resides immediately following the "text" segment. 
// 
//***************************************************************************** 
void Reset_Handler(void)
{
	unsigned long *pulSrc, *pulDest;

	//Copy the data segment initializers from flash to SRAM
	pulSrc = &_etext;
	for(pulDest = &__data_start__; pulDest < &__data_end__; )
	{
		*pulDest++ = *pulSrc++;
	}

	//Zero fill the bss segment
	for(pulDest = &__bss_start__; pulDest < &__bss_end__; )
	{
		*pulDest++ = 0;
	}
	
	//Call the application's entry point
	main();
}

/*----------------------------------------------------------------------------
  Default Handler for Exceptions / Interrupts
 *----------------------------------------------------------------------------*/
void Default_Handler(void)
{
  while(1);
}

#pragma weak FSP_IRQHandler
void FSP_IRQHandler(void)
{
	return;
}
